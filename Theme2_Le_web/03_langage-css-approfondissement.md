Langage CSS : approfondissement
===============================

<blockquote class="information">
    <p>Cette activité prolonge le travail entrepris sur le langage CSS dans la séance disponible à <a href="/snt/web/langage-css.php" target="_blank">cette adresse</a>. En particulier, on utilisera à nouveau le document HTML de cette séance.</p>
</blockquote>

# Appliquer un style : class et id

Avec ce qui a été vu dans la séance sur le langage CSS, **tous** les titres `h1` ont la même apparence, **tous** les paragraphes ont la même apparence, etc.

> Comment faire pour spécifier un style différent pour certains paragraphes précis ? 

Pour résoudre ce problème, on peut utiliser les deux attributs suivants qui fonctionnent sur toutes les balises :

- L’attribut `class` ;
- L’attribut `id`.

On utilisera l’attribut `class` lorsque l’on voudra cibler *plusieurs balises* alors que l’attribut `id` ne peut être utilisé qu’une seule fois, donc ne cibler qu’*une seule balise*.

## L'attribut `class`

Imaginons que l’on souhaite que les paragraphes sur les formations et les diplômes n’aient pas la même allure que les autres paragraphes. On va utiliser l’attribut `class` en modifiant le code HTML de la façon suivante sur la première ligne :

```html
<p class="formation-diplomes"><strong>Après le bac</strong>
<br>2 ans pour préparer le BTS design graphique, option communication et 
... 
</p>
```

On a ajouté le code `class="formations-diplomes"`, juste après le `<p` et avant le `>`. On vient d’associer la classe `formations-diplomes` à notre paragraphe. Le nom de la classe est celui que l’on souhaite, mais il est préférable de choisir un nom représentatif pour s’y retrouver.

💻 **Question 1** : Effectuez cette modification sur chaque paragraphe concernant les formations et les diplômes.

Il faut désormais compléter le fichier CSS pour indiquer le style des paragraphes identifiés par le nom `formations-diplomes`. Pour cela, on indique dans le fichier CSS le nom de la classe en commençant par un point :

```css
.formations-diplomes {
    color: #4696a8;
    text-align: justify;
}
```

💻 **Question 2** : Ajoutez ce code au fichier CSS et observez les changements (enregistrez les modifications et actualisez la page si nécessaire). La valeur `justify` permet aux bords gauche et droit du texte d’être alignés avec les bords gauche et droit du paragraphe.

## L'attribut `id`

L’attribut `id` fonctionne exactement de la même façon que `class`, mais ne peut être utilisé qu’une seule fois. Dans le code HTML, il faut remplacer `class` par `id` et dans le code CSS remplacer le `.` (point) par `#`.

Imaginons que l’on souhaite modifier l’apparence uniquement du dernier paragraphe, qui est différent de tous les autres de par son contenu. Dans le code HTML, on ajoute un identifiant au paragraphe en question dans la balise `<p>` :

```html
<p id="ressources">
```

Et dans le code CSS on précise son style ainsi :

```css
#ressources {
    margin-top: 30px; 
    padding-top: 20px;
    border-top: solid 1px black; /*ajout d'une bordure en haut*/
}
```

💻 **Question 3** : Ajoutez cela aux codes HTML et CSS et observez les changements.

# Les balises universelles `<div>` et `<span>`

Ce sont deux balises très utilisées (principalement la balise `<div>`). Elles n’ont aucune signification particulière mais servent à organiser la page et à faire des choses que l’on ne pourrait pas faire avec les attributs `class` et `id`.

Ces deux balises ont été inventées pour regrouper des éléments lorsqu'il n'est pas possible de le faire avec d'autres balises.

Il n’y a qu’une différence minime (mais significative !) entre les deux :

- La balise `<span>` est de type « **inline** » (= en ligne) qui sont des balises dont le contenu se place les uns à côtés des autres sur la page Web (sur la même ligne). On va par exemple l’utiliser au sein d’un paragraphe de texte pour sélectionner des mots.
- La balise `<div>` est de type « **block** » qui sont des balises qui créent des blocs qui sont placés les uns en-dessous des autres dans la page.

## La balise `<span>`

Par exemple, imaginons que nous voulions modifier uniquement le mot « SNT » dans le paragraphe suivant : 

```html
<p>La SNT, pour comprendre les technologies du numérique.</p>
```

Nous ne pouvons pas le faire avec `class` ou `id` car ces attributs doivent s’appliquer à une balise et qu’il n’y en a pas pour encadrer le mot « SNT ».

Nous allons utiliser la balise `<span>` pour modifier le mot « SNT ». Il suffit de l’insérer autour du mot de la façon suivante :

```html
<p>La <span class="mot">SNT</span>, pour comprendre les technologies du numérique.</p>
```

et on crée le code CSS suivant pour que ce mot devienne rouge et fasse 20 pixels de haut.

```css
.mot {	
    color: red;
    font-size: 20px;
}
```

Cela permet d'obtenir le rendu ci-dessous :

<img class="centre image-responsive bordure" src="data/balise_span.png" alt="illustration">

>Vous avez remarqué qu’il faut attribuer une `classe` (ou un `id`) à la balise `<span>` de façon à pouvoir lui appliquer un style CSS en utilisant le nom de la classe (ici `mot`).

💻 **Question 4** : Utilisez la balise `<span>` pour que la première lettre de chaque métier apparaisse dans une couleur grise (`#bbbbbb` par exemple) avec une taille de police de 40 pixels.

## La balise `<div>`

La balise `<div>` permet d’encadrer tout un bloc que l’on souhaite transformer. Généralement, il permet d’encadrer une ou plusieurs balises pour appliquer un style à l’ensemble de ces balises.

Imaginons que nous souhaitions appliquer un style à tout le texte décrivant un métier. Ce texte est composé d’un paragraphe, puis d’un titre et enfin d’un autre paragraphe :

```html
<p>À la fois artiste et informaticien, le webdesigner est capable de 
réaliser une interface web ergonomique et un design adapté au contenu 
d'un site Internet donné.</p>
    	
<h3>Les formations et les diplomes</h3>

<p class="formations-diplomes"><strong>Après le bac</strong>
<br>2 ans pour préparer le BTS design graphique, option communication et 
médias numériques, ou le DUT métiers du multimédia et de l'Internet, 
éventuellement complété par une licence professionelle en webdesign, 
création web, design numérique, services et produits multimédias... 
(1 an). Autre solution : préparer en 3 ans le DN MADE mention 
numérique.</p>
```

On va encadrer ces trois éléments avec une balise `<div>` de la façon suivante :

```html
<div class="description-metier">
    <p>À la fois artiste ...</p>      
    <h3>Les formations et les diplomes</h3>
    <p class="formations-diplomes"><strong>Après le bac</strong>
    <br>2 ans pour...</p>
</div>
```

Et on définit son style CSS de la façon suivante :

```css
.description-metier {
    margin-left: 15px;  /* marge extérieure gauche */
    margin-right: 15px;  /* marge extérieure droite */
    padding: 10px;  /* marges intérieures */
    background-color: white;  /* couleur du fond */
    border: solid 7px #bbbbbb;  /* bordure (type, épaisseur, couleur) */
    border-radius: 30px;  /* angles arrondis */
}
```

💻 **Question 5** : 5.	Utilisez ce code et appliquez ce style à chacun des métiers de votre page Web. Observez le résultat.

# Créer des apparences dynamiques

Il est possible de modifier l’apparence d’éléments grâce à CSS. On utilise pour cela des pseudo-classes (ce nom n’a pas beaucoup d’importance).

La pseudo-classe sans doute la plus utilisée s’appelle `:hover` qui signifie « survoler ». On va l’utiliser pour modifier des apparences au survol de certains éléments.

Par exemple, si on souhaite modifier l’apparence des titres `<h2>` lors du survol de la souris, il suffit d’ajouter au code CSS :

```css
h2:hover {
    color: white;
    background-color: gray;
}
```

💻 **Question 6** :  Ajoutez ce code au fichier CSS et observez le résultat.

En pratique, la pseudo-classe `:hover` est surtout utilisée au survol des liens et des boutons, souvent pour montrer qu'il est possible de cliquer sur l'élément survolé. Observez par exemple, les changements d'apparence sur cette page lors du survol de la souris des éléments du menu tout en haut.

Les liens sont par défaut colorés et soulignés. Si on souhaite par exemple que les liens ne soient pas soulignés et qu’ils deviennent rouges au survol de la souris, il suffit d’ajouter au code CSS les lignes suivantes :

```css
/* liens non survolés */
a {
    text-decoration: none;  /* pour ne pas souligner les liens hypertextes */
}

/* liens survolés */
a:hover {
    color: red;  /* pour colorier en rouge lors du survol */
}
```

💻 **Question 7** : Ajoutez ces lignes au code CSS et observez le résultat au survol des liens.

# Positionner des éléments avec `flexbox` et `grid`

Tout ce que nous avons vu jusqu'à présent ne permet de gérer le positionnement de plusieurs éléments sur une page, ce qui est indispensable sur la plupart des pages Web.

Pour positionner les éléments sur la page, il existe deux modèles natifs : "Flexbox" et "Grid Layout".

Pour découvrir et apprendre ces deux méthodes de positionnement, vous pouvez utiliser les jeux en ligne suivants qui vous guident pas à pas :
- **Apprendre Flexbox avec Froggy la grenouille** : [https://flexboxfroggy.com/#fr](https://flexboxfroggy.com/#fr)

<a href="https://flexboxfroggy.com/#fr" target="_blank">
    <img class="centre image-responsive" alt="Flexbox Froggy" src="data/flexboxfroggy.png" width="700">
</a>
<p class="legende">Source : <a href="https://github.com/thomaspark/flexboxfroggy/">https://github.com/thomaspark/flexboxfroggy/</a>.</p>

- **Apprendre Grid Layout en cultivant les carottes de votre jardin avec Grid Garden** : [https://cssgridgarden.com/#fr](https://cssgridgarden.com/#fr)

<a href="https://cssgridgarden.com/#fr" target="_blank">
    <img class="centre image-responsive" alt="Grid Garden" src="data/cssgridgarden.png" width="700">
</a>
<p class="legende">Source : <a href="https://github.com/thomaspark/gridgarden/">https://github.com/thomaspark/gridgarden/</a>.</p>

Il existe aussi les tutoriels intéressants de Grafikart :
- sur Flexbox : [https://grafikart.fr/tutoriels/flexbox-806](https://grafikart.fr/tutoriels/flexbox-806)
- sur Grid Layout : [https://grafikart.fr/tutoriels/grid-css-1002](https://grafikart.fr/tutoriels/grid-css-1002)
- Float, Flex ou grid ? : [https://grafikart.fr/tutoriels/float-flex-grid-2017](https://grafikart.fr/tutoriels/float-flex-grid-2017)

# Sélectionner des éléments

Lorsqu'une page Web devient assez conséquentes, les balises deviennent très nombreuses, et imbriquées les unes dans les autres. Et cela devient plus compliqué pour sélectionner certains éléments en CSS. Pour apprendre à sélectionner les éléments, le jeu interactif **CSS Diner** est intéressant (en anglais) : [https://flukeout.github.io/#](https://flukeout.github.io/#).


<a href="https://flukeout.github.io/#" target="_blank">
    <img class="centre image-responsive" alt="CSS Diner" src="data/cssdiner.jpg" width="700">
</a>
<p class="legende">Source : <a href="https://github.com/flukeout/css-diner/">https://github.com/flukeout/css-diner/</a>.</p>


---
**Références** :
- Livre *Réalisez votre site web avec HTML 5 et CSS 3*, Mathieu Nebra, OpenClassrooms, éditions EYROLLES ;
- Cours OpenClassrooms : https://openclassrooms.com/fr/courses/1603881-apprenez-a-creer-votre-site-web-avec-html5-et-css3

---
Germain Becker, Lycée Emmanuel Mounier, Angers
![licence CC BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)







