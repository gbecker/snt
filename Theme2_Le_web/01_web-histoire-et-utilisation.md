Le Web : histoire et utilisation
================================

Dans cette séance, on définit ce qu'est le Web et on donne des **repères historiques** importants. On décrit aussi comment les utilisateurs peuvent naviguer sur le Web grâce aux **navigateurs**, aux **liens hypertextes** et aux **URL**. Enfin, comme le Web offre à tous la possibilité de publier du contenu facilement et instantanément, le **droit d'auteur** n'est souvent pas respecté. On rappelle quelques règles et on propose quelques solutions pour y remédier, notamment grâce aux **licences Creative Commons**. 

# La petite histoire du Web et son utilisation

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/web/data/Web_histoire_navigateurs_url.pdf"
    class = "centre"
    width="800"
    height="600"
    style="border: none;">
    </iframe>
</div>
<p class="impression">
Source : <a href="https://info-mounier.fr/snt/web/data/Web_histoire_navigateurs_url.pdf">https://info-mounier.fr/snt/web/data/Web_histoire_navigateurs_url.pdf</a>
</p>

# Publier dans les règles

Dans l’histoire de la communication le Web est une révolution : il a en particulier ouvert à tous la possibilité et le droit de publier et permet une diffusion facile et immédiate de plusieurs milliards de documents par jour (articles, images, vidéos, musiques, etc.) qui rend très compliqué le respect du droit d’auteur. L’objectif ici, sans dériver vers un cours de droit, est de : 

- comprendre les règles à respecter lorsque l’on souhaite publier du contenu sur le Web ou réutiliser un contenu déjà publié
- connaître des moyens pour publier librement du contenu et pour trouver du contenu libre de droits

## Quelques définitions

Voici un extrait de la définition du droit d’auteur donnée par Wikipédia :

<div class="important">
    <p>Le <strong>droit d’auteur</strong> est l’ensemble des droits dont dispose un auteur ou ses ayants droit (héritiers, sociétés de production), sur ses œuvres originales définissant notamment l'utilisation et à la réutilisation de ses œuvres sous certaines conditions.</p>
</div>

<!-- <blockquote class="citation">
    <p>Le droit d’auteur est l’ensemble des droits dont dispose un auteur ou ses ayants droit (héritiers, sociétés de production), sur ses œuvres originales définissant notamment l'utilisation et à la réutilisation de ses œuvres sous certaines conditions.</p>
</blockquote> -->

✍️ **Question 1** : Rendez-vous sur l'article Wikipédia concernant le <a href="https://fr.wikipedia.org/wiki/Droit_d%27auteur" target="_blank">droit d'auteur</a>, repérez dans le début de l'article les types d'oeuvres protégées par le droit d'auteur. Listez-les !

✍️ **Question 2** : Toujours en vous aidant du début de l'article, donnez les deux types de droits dont est composé le droit d'auteur.

✍️ **Question 3** : Toujours sur cet article, cliquez sur le lien hypertexte vous permettant de vous rendre sur l'article Wikipédia concernant le *droit moral*. Ces droits moraux comprennent trois droits, listez-les et expliquez chacun en une phrase. 

## Ce qu'on n'a pas le droit de faire !

<div class="a-faire titre" markdown="1">

### ✏️ À faire

▶️ Regardez la vidéo suivante **jusqu’à 5’43** pour vous informer sur ce que vous n'avez pas le droit de faire (et que vous faites sans doute), puis répondez aux questions qui suivent.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="j5xJS6RpRaA" width="560" height="315" end="343" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : <a href="https://youtu.be/j5xJS6RpRaA?end=343" target="_blank">https://youtu.be/j5xJS6RpRaA?end=343</a>.
</p>

</div>

✍️ **Question 4** : Listez les six premiers cas évoqués dans la vidéo pour lesquels la réutilisation d'une photo trouvée sur le Web est interdite (sans demander l'autorisation à l'auteur).

✍️ **Question 5** : Selon vous (la réponse n'est pas la vidéo précédente !), quelles difficultés peut-on rencontrer pour demander à l’auteur son autorisation pour utiliser une photo (ou un document au sens plus large) publié sur le Web ?


## Les licences Creative Commons

<img class="centre image-responsive" src="data/CC-logo.svg" width="300">
<p class="legende">
    Crédit : Creative Commons, Domaine public, source : <a href="https://commons.wikimedia.org/wiki/File:CreativeCommons_logo_trademark.svg" target="_blank">Wikimedia Commons</a>.
</p>

Publier du contenu sous licence gratuite permet une réutilisation de ce contenu sans autorisation et sous certaines conditions. Il n’est donc plus nécessaire de contacter l’auteur (ou ses ayants droit) pour utiliser sa création. Les licences Creative Commons (abrégé CC) sont les plus connues des licences gratuites.

<div class="a-faire titre" markdown="1">

### ✏️ À faire

▶️ Regardez la vidéo ci-dessous pour comprendre les licences Creative Commons, puis répondez aux questions qui suivent.

<div class="video-responsive">
    <div class="peertube_iframe" title="Comprendre les Creative Commons pour une utilisation pédagogique" width="560" height="315" src="https://tube.reseau-canope.fr/videos/embed/17d63f12-2a68-4110-8943-dba74582befd?warningTitle=0&amp;p2p=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></div>
</div>
<p class="impression">
    Source : <a href="https://tube.reseau-canope.fr/w/3WHSEvpvBhkzA93BZPUyVc">https://tube.reseau-canope.fr/w/3WHSEvpvBhkzA93BZPUyVc</a>
</p>

</div>



✍️ **Question 6** : Quelles sont les 4 options des licences Creative Commons ?

✍️ **Question 7** : Définissez chacune d’elle par une phrase pour expliquer son rôle et donnez sa signification anglaise (pour mieux les retenir).
    
## Des banques d’images en ligne

Il existe beaucoup de banques d’images en ligne. Parmi les plus connues on peut citer Pixabay, Wikimedia Commons et Flickr.

<div class="a-faire titre" markdown="1">

### ✏️ À faire

👓 Lisez ce qui suit en faisant ce qui est proposé pour utiliser les diffférents banques d'images proposées, en vue d'une utilisation ultérieure.

</div>

### Wikimedia Commons

*Wikimedia Commons* est une médiathèque d'images (mais aussi de sons, de médias audiovisuels, etc.) ne proposant que des images sous licence libre, beaucoup étant sous licence Creative Commons. 

<img class="centre image-responsive" src="data/Commons-logo-en.svg" width="150">
<p class="legende">
    Crédit : <a href="https://commons.wikimedia.org/wiki/File:Commons-logo-en.svg" target="_blank">Wikimedia Foundation</a>, Licence <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.fr" target="_blank">CC BY-SA 3.0</a>.
</p>

🖥️ **À faire** : Connectez-vous sur [https://commons.wikimedia.org/wiki/Accueil](https://commons.wikimedia.org/wiki/Accueil?uselang=fr) et faites une recherche en tapant le mot « chaton ».  

1. Cliquez alors sur l'une de ces images et repérez sous cette image sa licence.
2. Cliquez sur le bouton "Plus de détails" (ou sur le titre de l'image) et repérez comment télécharger l'image pour des utilisations ultérieures.
3. Après avoir cliqué sur "Plus de détails", cliquez sur "Utiliser ce fichier *sur un site internet*" et repérez la ligne *Crédit à l'auteur* qui est le texte à copier pour citer l'auteur et la licence utilisée.

><span class="emoji">💡</span> C'est ainsi très facile de créditer un auteur, puisqu'il suffit de faire un copier-coller de cette ligne. On peut même l'avoir au format HTML en cochant la case correspondante.

### Flickr

*Flickr* est une banque d'images de plusieurs dizaines de milliards de photos. Attention, les photos ne sont pas toutes sous licence libre mais il est possible de filtrer celles sous licence Creative Commons. 

<img class="centre image-responsive" src="data/Flickr_wordmark.svg" width="250">
<p class="legende">
    Crédit : Domaine public, source : <a href="https://fr.wikipedia.org/wiki/Flickr#/media/Fichier:Flickr_wordmark.svg" target="_blank">Wikipedia</a>.
</p>

🖥️ **À faire** :  Connectez-vous sur [https://www.flickr.com/](https://www.flickr.com/) et faites une recherche en tapant le mot « chaton ».  
    
1. En haut à gauche, sélectionnez *Tous les Creative Commons* à la place de *Toute licence*. Cela permet de n'afficher que les images sous licence Creative Commons (CC).
2. Cliquez alors sur l'une de ces images et repérez en bas à droite de l'image (sous la date de prise de la photo) la licence Creative Commons de l'image donnée par les pictogrammes vus dans la partie précédente.
3. Repérez comment télécharger l'image (il faut trouver le logo habituel de "Téléchargement") pour des utilisations ultérieures.

### Pixabay

La banque d'images *Pixabay* ne contient quant à elle que des images libres de droits et gratuites. Elles sont sous licence "Pixabay License" qui permet de les utiliser gratuitement et de faire ce que l’on veut avec (ou presque).

>Il n'est donc théoriquement pas nécessaire de citer leur auteur, *mais cela s'avère tout de même une bonne pratique*, de même que de citer la source (Pixabay) pour que d'autres puissent retrouver l'image.

<img class="centre image-responsive" src="data/Pixabay-logo.svg" width="300">
<p class="legende">
    Crédit : Domaine public, source : <a href="https://fr.wikipedia.org/wiki/Pixabay#/media/Fichier:Pixabay-logo.svg" target="_blank">Wikipedia</a>.
</p>

🖥️ **À faire** : Connectez-vous sur [https://pixabay.com/fr/](https://pixabay.com/fr/) et faites une recherche en tapant le mot « chaton ».  

1. Cliquez alors sur l'une de ces images et trouvez la licence de l'image.
2. Repérez où se trouve le nom de l'auteur (pour pouvoir le citer le cas échéant) et comment la télécharger pour des utilisations ultérieures.

### Autres banques d'images

Il existe d'autres banques d'images en ligne sur lesquelles les images sont libres de droit. Parmi les plus utilisées, on peut par citer [Unsplash](https://unsplash.com/fr) et [Pexels](https://www.pexels.com/fr-fr/).

>Il peut être intéressant de regarder quelles sont les conditions des licences des images sur ces sites, comment télécharger une image et comment créditer l'auteur (cela reste fortement conseillé, même si ce n'est pas obligatoire).


<div class="a-supprimer">

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>