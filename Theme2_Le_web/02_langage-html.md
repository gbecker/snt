# Créer une page Web avec le langage HTML

<img class="centre image-responsive" width="200" src="data/HTML5_logo.svg" alt="logo HTML5">
<p class="legende">
    Crédit : <a href="https://commons.wikimedia.org/wiki/File:HTML5_logo_and_wordmark.svg">W3C</a>, <a href="https://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>, via Wikimedia Commons
</p>

Les pages Web sont écrites dans le langage **HTML** (*HyperText Language Markup*, soit « langage de balisage d'hypertexte ») qui est un langage de **balises** (*markup* signifie balise en anglais). Dans cette séance, vous allez découvrir ce qu'est une balise puis vous écrirez votre première page HTML.

<blockquote class="information">
    <p>Le langage HTML a été inventé et développé par Tim Berners-Lee et Robert Cailliau au début des années 1990. C'est en 1993 que la version initiale de HTML apparaît. Depuis 2007, c'est la version 5 de HTML qui est utilisée, on parle de HTML 5.</p>
</blockquote>

# Découverte des balises

Une balise sert à **structurer** les différents éléments d'un ensemble. Avant de s'intéresser aux balises d'une page Web, illustrons le principe des balises en programmant des élèves. Pour jouer à cela, il faut :
- huit élèves qui viennent s'assoir côte à côte face à la classe  ;
- un (ou plusieurs) programmeur(s) qui disposent de 3 types de balises :
    - `<bras gauche levé>` et `<fin bras gauche levé>`
    - `<bras droit levé>` et `<fin bras droit levé>`
    - `<debout>` et `<fin debout>`

<span class="emoji">👉</span> **Étape 1** : Le programmeur doit placer les balises `<bras gauche levé>` et `<fin bras gauche levé>` pour que tous les élèves lèvent le bras gauche, puis uniquement certains d'entre eux.

<span class="emoji">👉</span> **Étape 2** : Le programmeur doit placer les balises à disposition pour que certains élèves aient le bras droit levé et d'autres le bras gauche, aucun ne doit lever les deux bras.

<span class="emoji">👉</span> **Étape 3** : Le programmeur doit placer les balises à disposition pour que certains élèves aient les deux bras levés mais qu'au moins un élève ait un seul bras levé. Attention, il est interdit de "croiser" les balises.

<span class="emoji">👉</span> **Étape 4** : Le programmeur doit placer les balises de manière à obtenir la configuration suivante :

<img class="centre image-responsive" src="data/silhouettes_ex1.png" alt="illustration">

# Identifier les principales balises HTML

Pour écrire une page Web, on utilise le **langage HTML**. Concrètement, il faut écrire un fichier HTML dont l'extension est `.html`. Pour écrire ce fichier, on utilise différentes balises.

Pour comprendre le rôle de certaines balises du langage HTML, on se propose de comparer le contenu d’un fichier HTML avec le rendu dans un navigateur Web de la page.

## Code HTML de la page

Voici le contenu du fichier HTML :

```html
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Une page exemple</title>
    </head>
    <body>
        <h1>Le Web</h1>
        <h2>Son inventeur</h2>
        <img src="https://live.staticflickr.com/7382/27297046510_89dd7a43ac_q.jpg" 
             alt="une photo de Tim Berners-Lee">
        <p><strong>Timothy John Berners-Lee</strong>, né le 8 juin 1955 à Londres,
        est un informaticien britannique, principal inventeur du World Wide Web (WWW)
        en 1989 lors de ses travaux au <em>CERN</em>.</p>
        <p>À partir de juillet 1990, il invente et développe avec l'ingénieur belge
        <a href="https://fr.wikipedia.org/wiki/Robert_Cailliau">Robert Cailliau</a>,
        les trois technologies fondatrices du Web :</p>
        <ul>
            <li>les adresses Web sous forme d'<strong>URL</strong> ;</li>
            <li>le protocole de communication <strong>HTTP</strong> ;</li>
            <li>et le langage <strong>HTML</strong> qui utilise des balises
            pour structurer les pages Web.</li>
        </ul>
        <p>En 2004, il est fait chevalier par la reine Élisabeth II pour ce travail
        et son nom officiel devient Sir Timothy John Berners-Lee. Il est lauréat
        du <a href="https://fr.wikipedia.org/wiki/Prix_Turing">prix Turing</a>
        en 2016 (l'équivalent du prix Nobel en informatique).</p>
    </body>
</html>
```

## Affichage de la page par un navigateur

Voici la page interprétée et affichée par un navigateur :

<img class="centre image-responsive bordure" src="data/capture_ecran_identifier_balises.png" alt="capture d'écran d'une page Web">

✍️ **Question 1** : Quels sont les symboles qui encadrent toutes les balises ?

✍️ **Question 2** : En HTML, quel symbole permet de distinguer une balise « de fin » d’une balise « de début » ?

✍️ **Question 3** : En comparant attentivement le code HTML et l'affichage de la page, complétez le tableau qui suit :

| Balise | Rôle |
| --- | --- |
| `<h1>` | |
| | Titre moins important |
| | Paragraphe |
| `<strong>` | |
| | Texte en italique |
| `<a>` | |
| | Image |
| | Liste non ordonnée d'éléments |
| `<li>` | |
| `title` | | 

La structure générale d'une page Web est toujours la suivante :

```html
<!DOCTYPE html>
<html>
    <head>
        ...
    </head>
    <body>
        ...
    </body>
</html>
```

La première ligne `<!DOCTYPE html>` indique que le document est au format HTML. Ensuite, le document commence toujours par la balise `<html>` et se termine par `</html>`. Entre ces deux balises on trouve deux parties délimitées par les balises `<head>` et `<body>`.

✍️ **Question 4** : Le code HTML correspondant au contenu affiché à l'écran doit-il être placé dans la balise `<head>` ou dans la balise `<body>` ? Entourez dans le code HTML toute la partie qui est affichée dans la fenêtre du navigateur.

<blockquote class="information">
    <p>La partie délimitée par les balises <code>&lt;head&gt;</code> et <code>&lt;/head&gt;</code> s'appelle l'<em>en-tête</em> de la page Web. Elle contient des <em>métadonnées</em>, c'est-à-dire des informations générales sur le document comme son titre, l'encodage des caractères, des liens vers des feuilles de style ou des scripts.</p>
    <p>La partie délimitée par les balises <code>&lt;body&gt;</code> et <code>&lt;/body&gt;</code> s'appelle le <em>corps</em> de la page Web.</p>
</blockquote>


# Création d'une première page Web

<div class="important">
    <p><span class="emoji grand">⚠️</span> Les instructions de l'activité sont données ci-dessous mais tout est à faire sur Capytale en utilisant le code ou le lien fourni par le professeur.</p>
</div>

Après avoir ouvert l'activité dans Capytale, vous devez voir une fenêtre comme ci-dessous.

<img class="centre image-responsive bordure" src="data/apercu_capytale.png" alt="capture d'écran d'une page Web">

## Titre et paragraphes

Vous pouvez masquer les consignes dans Capytale puisque celles-ci se trouvent ci-dessous.

<span class="emoji grand">📣</span> **Pensez à sauvegarder régulièrement votre travail !**

**Tout le code** donné par la suite **est à écrire entre les balises `<body>` et `</body>`**. La page Web s'affichera au fur et mesure dans la zone grise en bas (le fond de cette zone normalement devrait être blanc mais Capytale a fait un autre choix, ce n'est pas important pour cette séance)

💻 **Question 5** : Écrivez (dans les balises `<body>`) les deux lignes suivantes :

```html
<h1>Mon premier titre</h1>
<p>Mon premier paragraphe de ma première page Web !</p>
<p>Et voici un autre paragraphe.</p>
```

<blockquote class="information">
    <p>Vous devez remarquer que la balise fermante est autocomplétée dès que l'on a écrit l'ouvrante correspondante : cela permet de gagner du temps. On peut aussi écrire simplement <code>h1</code> puis appuyer sur la touche "Tabulation" pour obtenir <code>&lt;h1&gt;&lt;/h1&gt;</code> et aller ainsi encore plus vite.</p>
</blockquote>

## 🔗 Insérer un lien hypertexte vers un autre site

💻 **Question 6** : Ajoutez à la suite, toujours entre les balises `<body>`, le code HTML suivant :

```html
<h2>Un site génial !</h2>
<p>Voici un lien vers un site très important : 
<a href="https://fr.wikipedia.org">Wikipédia</a>.</p>
```

✍️ **Question 7** : Répondez aux questions suivantes :
- À quoi sert la balise `<h2>` utilisée ici ?
- Quelle balise a permi d'écrire un lien hypertexte ?
- Quel est le texte cliquable affichée à l'écran ?
- Vers quelle URL pointe cet hyperlien ? 

✍️ **Question 8** : Quel code HTML faut-il écrire pour obtenir un lien hypertexte dont le texte affiché à l'écran est `Site pour la SNT` et qui pointe vers la page `https://info-mounier.fr/snt/web` ?

## 🎞️ Insérer une image

💻 **Question 9** : Ajoutez à la suite, toujours entre les balises `<body>`, le code suivant :

```html
<h2>Une image toute mignonne</h2>
<img src="" alt="une image de chat">
```

**Remarque** : La balise `<img>` est utilisée pour insérer une image dans une page Web. Cette balise possède deux *attributs* : 
- `src` (pour *source*) qui indique le chemin vers l'image que l'on veut insérer : ce chemin peut être l'URL d'une image hébergée sur le Web ou un chemin sur un disque ;
- `alt` (pour *alternative*) qui est un texte alternatif décrivant l'image (`"une image de chat"` dans notre cas)

Pour le moment, comme la valeur de l'attribut `src` n'est pas définie, vous devriez voir apparaître à l'écran le texte alternatif à la place de l'image. On va remédier à cela tout de suite !

💻 **Question 10** : Rendez-vous sur le site <a href="https://pixabay.com/fr/" target="_blank">Pixabay</a>, cherchez puis cliquez sur une image de chaton. Faites ensuite un clic droit sur l'image et sélectionnez "Copiez l'adresse de l'image". Collez alors cette URL entre les guillemets de l'attribut `src` (vous devez avoir quelque chose comme ceci : `src="url-de-votre-image"`). Si l'image s'affiche c'est que vous avez réussi, sinon reprenez la procédure attentivement.

✍️ **Question 11** : A-t-on le droit d'utiliser librement cette image sans demander l'autorisation à l'auteur ? Expliquez rapidement.


## ▶️ Intégrer une vidéo

💻 **Question 12** : Ajoutez le titre de niveau 2 (balise `h2`) intitulé "Une vidéo intégrée" à la suite de votre page.

💻 **Question 13** : Dans un **nouvel onglet** du navigateur, allez sur YouTube et lancez une vidéo de votre choix. Cliquez droit sur la vidéo puis cliquez sur « Copier le code d’intégration ». Collez maintenant (Ctrl+V) ce code à la suite de votre code HTML (toujours avant la balise `</body>`). Vous venez d’intégrer une vidéo dans votre page Web !

Voici le *genre de code* correspondant à un code d'intégration :

```html
<iframe width="853" height="480" src="https://www.youtube.com/embed/PbCG_BeL5dY" 
title="Tim Berners-Lee, le génie inventeur du web" frameborder="0" 
allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope;
picture-in-picture" allowfullscreen></iframe>
```

**Analyse** :

- La balise `<iframe>` permet d’intégrer une autre page HTML (qui est ici une vidéo) dans la page HTML courante ;
- `width="853" height="480"` sont les attributs qui définissent la largeur (width) et la hauteur (height) en pixel de la page à intégrer ;
- `src="https://www.youtube.com/embed/PbCG_BeL5dY` est l'attribut qui définit l’URL de la page à intégrer (*src* pour source).
- On ne parle pas du reste ici.

<div class="important">
    <p><span class="emoji grand">⚠️</span> Une fois terminé, <strong>n'oubliez pas de rendre votre travail</strong> en cliquant sur l'icône correspondante en haut à gauche. Attention, il n'est plus possible de le modifier une fois rendu (vous pourrez néanmoins y accéder en consultation).</p>
</div>

<blockquote class="information">
    <p>Vous avez désormais toutes les connaissances pour démarrer le projet de création d'une page Web sur une personnalité (voir la rubrique "Projet" sur le site).</p>
</blockquote>

