# Le style avec CSS

<img class="centre image-responsive" width="150" src="data/CSS3_logo.svg" alt="logo CSS3">

<p class="legende">
  Crédit : <a href="https://commons.wikimedia.org/wiki/File:CSS3_logo_and_wordmark.svg">Rudloff</a>, <a href="https://creativecommons.org/licenses/by/3.0">CC BY 3.0</a>, via Wikimedia Commons
</p>

Dans cette séance vous allez découvrir le **langage CSS** (*Cascading Style Sheets* en anglais, ou « feuilles de style en cascade ») qui permet de définir le **style graphique d’une page** et donc de rendre plus "jolie" une page Web.

>Par exemple, voici une lien vers une <a href="/index_sans_css.php" target="_blank">page sans CSS</a> et la même  <a href="/" target="_blank">avec CSS</a>, c'est assez parlant !

<blockquote class="information">
    <p>Le langage CSS est le fruit de travaux de différentes personnes entre 1993 et 1996. La version initiale de CSS a vu le jour en décembre 1996, soit trois années après celle du langage HTML.</p>
</blockquote>

Un **fichier CSS** contient des **règles de mise en forme**. Par exemple, on pourra y définir que tous les titres de niveau 1 (balise `<h1>`) sont de telle couleur, de telle police, sont centrés, etc.

L’utilisation d’un fichier CSS contribue à homogénéiser la présentation d’une page Web et à séparer le contenu (HTML) de la forme (CSS). Il suffit de le relier au fichier HTML dans les en-têtes de ce dernier.

# Écriture de règles CSS

<div class="important">
    <p><span class="emoji grand">⚠️</span> Les instructions de l'activité sont données ci-dessous mais tout est à faire sur Capytale en utilisant le code ou le lien fourni par le professeur.</p>
</div>

Après avoir ouvert l’activité dans Capytale, vous devez voir une fenêtre comme dans la séance sur le HTML. Évidemment, **le code CSS sera a écrire dans la fenêtre "CSS"** !

Vous pouvez masquer les consignes dans Capytale puisque celles-ci se trouvent ci-dessous.

><span class="emoji grand">📣</span> **Pensez à sauvegarder régulièrement votre travail !**

Une règle CSS se construit de la façon suivante :

<img class="centre image-responsive bordure" width="200" src="data/anatomie_regle_css.png" alt="anatomie d'une règle CSS">

**Analyse** : 
- Le *sélecteur* est l'élément HTML que l'on veut styliser (ici `p` donc tous les paragraphes). 
- Ensuite, *entre accolades* on écrit la *propriété* de style que l'on veut appliquer et sa *valeur* (ici `color: red;` signifie que l'on veut que la couleur soit rouge).
- Une telle déclaration se termine par un *point-virgule*.

Vous allez expérimenter et comprendre tout cela tout de suite !

💻 **Question 1** : Dans la zone réservée au CSS de l'activité sur Capytale, écrivez le code suivant et observez l'impact sur le style de la page.

```css
h1 {
    text-align: center;
    color: blue;
    font-size: 40px;
}

h2 {
    color: white;
    background-color: black;
}

p {
    font-family: Verdana;
}
```

✍️ **Question 2** : Indiquez alors le rôle de chacune des lignes de ce code CSS.

💻 **Question 3** : Modifiez le code CSS pour remplacer la police Verdana des paragraphes en la police Arial.

✍️💻 **Question 4** : Que faudrait-il ajouter comme code pour que les titres de niveau 3 (balises `<h3>`) soient centrés et de police Arial ? Faites-le et vérifiez en regardant les changements sur la page.

💻 **Question 5** : Ajoutez le code suivant au début de votre fichier CSS (inutile de recopier ce qui se trouve entre `/*` et `*/` qui sont des commentaires non pris en compte) et observez les changements.

```css
body {
    width: 800px;  /* largeur de 800 pixels */
    margin: auto;  /* marges automatiques pour centrer horizontalement */
    background-color: rgb(240, 255, 255);  /* couleur du fond */  
}
```

*N’hésitez pas à essayer de modifier les valeurs pour observer les changements !*

💻 **Question 6** : **Ajoutez** les règles suivantes **pour le sélecteur `h2`** et observez les changements pour les titres de niveau 2 :

```css
padding-left: 20px;
padding-top: 5px;
padding-bottom: 5px;
border-radius: 10px;
```

**Explications** :
- L'unité de mesure `px` signifie "pixels" (`20px` signifie donc 20 pixels).
- `padding` désigne les marges intérieures d’un élément. Ici on a modifié les marges intérieures gauche (`padding-left`), haute (`padding-top`) et basse (`padding-bottom`) des balises `<h2>`. 
- `border-radius` définit des coins arrondis pour la bordure d’un élément.

<img class="centre image-responsive" src="data/illustration_css_1.png" alt="illustration">

💻 **Question 7** : **Ajoutez** les règles suivantes **pour le sélecteur `p`** et observez les changements

```css
margin-left: 20px;
margin-right: 100px;
```

**Explications** : `margin` désigne les marges extérieures d'un élément. Ici on a modifié les marges gauche (`margin-left`) et droite (`margin-right`) des balises `<p>`. 

<img class="centre image-responsive" src="data/illustration_css_2.png" alt="illustration">

<div class="important">
    <p><span class="emoji grand">⚠️</span> Une fois terminé, <strong>n'oubliez pas de rendre votre travail</strong> en cliquant sur l'icône correspondante en haut à gauche. Attention, il n'est plus possible de le modifier une fois rendu (vous pourrez néanmoins y accéder en consultation).</p>
</div>

<blockquote class="information">
    <p>Vous avez désormais toutes les connaissances pour poursuivre le projet de création d'une page Web sur une personnalité en stylisant votre page Web de la plus belle des manières (voir la rubrique "Projet" sur le site).</p>
</blockquote>

# Pour aller plus loin ...

<div class="important">
    <p><span class="emoji grand">⚠️</span> Cette partie est hors programme donc facultative.</p>
</div>

Il est bien sûr impossible de lister ici toutes les possibilités offertes par le langage CSS. Si vous voulez aller plus loin et en apprendre davantage, cliquez sur le lien suivant : <a href="langage-css-approfondissement" target="_blank">Pour aller plus loin</a>.


<div class="a-supprimer">

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>

