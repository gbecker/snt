# Les moteurs de recherche

<img class="centre image-responsive" width="400" src="data/search_engines.svg" alt="illustration moteurs de recherche">

Dans cette séance, nous allons étudier les **principes et les usages des moteurs de recherche**. Nous nous attarderons, à la fin, sur l'algorithme **PageRank** utilisé par Google pour classer les résultats avec pertinence en tenant compte de la *popularité* des pages Web.


# Diaporama d'introduction

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/web/data/Moteurs_de_recherche.pdf"
        class = "centre"
        width="800"
        height="600"
        style="border: none;">
    </iframe>
</div>
<p class="impression">
    Source diaporama : <a href="https://info-mounier.fr/snt/web/data/Moteurs_de_recherche.pdf">https://info-mounier.fr/snt/web/data/Moteurs_de_recherche.pdf</a>
</p>

# Les résultats fournis par un moteur de recherche

<img class="centre image-responsive" width="200" src="data/search_re.svg" alt="illustration moteurs de recherche">

✍️ **Question 1** : Utilisez votre navigateur habituel et saisissez dans la barre d'URL les termes "Emmanuel Mounier" puis validez. Quel est le moteur de recherche utilisée par défaut dans votre navigateur ?

✍️ **Question 2** : Effectuez la recherche "Emmanuel Mounier" sur les trois moteurs de recherche proposés et complétez le tableau.

| Moteur de recherche | Présence d'une annonce de publicité en tête des résultats | Position du premier lien vers Wikipédia | Position du premier lien vers le site du lycée |
| --- | --- | --- | --- |
| Google | | | |
| Qwant | | | |
| DuckDuckGo | | | |

✍️ **Question 3** : Comment expliquer que le classement des résultats diffère d'un moteur de recherche à l'autre ?

✍️ **Question 4** : Pourquoi les articles pointant vers Wikipédia arrivent-ils souvent bien classés ?

✍️ **Question 5** : Comment font les entreprises pour que leur page arrive en haut de classement en tant qu'annonce publicitaire ? Pourquoi font-elles cela ?

✍️ **Question 6** : En observant les pages d'accueil des moteurs de recherche Qwant et DuckDuckGo, expliquez quelle est leur principale différence avec le moteur de recherche Google.

# L'algorithme PageRank de Google

<img class="centre image-responsive" width="200" src="https://upload.wikimedia.org/wikipedia/commons/thumb/2/2f/Google_2015_logo.svg/langfr-1920px-Google_2015_logo.svg.png" alt="logo de Google">

## La petite histoire de Google

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/web/data/Google_PageRank.pdf"
        class = "centre"
        width="800"
        height="600"
        style="border: none;">
    </iframe>
</div>

<p class="impression">
    Source diaporama : <a href="https://info-mounier.fr/snt/web/data/Google_PageRank.pdf">https://info-mounier.fr/snt/web/data/Google_PageRank.pdf</a>
</p>

## Calculer la popularité d'une page avec PageRank

L'algorithme **PageRank** est un algorithme permettant de **calculer la popularité** des pages Web afin de classer les pages de réponses avec beaucoup de pertinence. Dans cette partie, vous allez exécuter cet algorithme *à la main*.

<blockquote class="information">
    <p>Ce n'est qu'un des algorithmes parmi l'ensemble de ceux utilisés par Google pour classer les pages, mais sans doute celui qui a fait de Google le moteur de recherche qu'il est devenu.</p>
</blockquote>

On suppose qu'un moteur de recherche a enregistré 6 pages web. Voici le schéma représentant ces 6 pages ainsi que les hyperliens entre elles symbolisés par des flèches.

<img class="centre image-responsive" src="data/graphe_pagerank.svg" alt="graphe de l'exercice">

Ainsi, la page A possède un hyperlien vers la page E, la page B possède deux hyperliens sortants vers les pages A et E, etc.

Les règles du jeu pour exécuter l'algorithme PageRank sont très simples :

- On met un pion sur une page au départ.
- On choisit au hasard une destination parmi les hyperliens sortants de cette page.
- On recommence le point précédent à partir de la page atteinte, et ainsi de suite

✍️ **Question 7** : On suppose que pour simuler le choix au hasard parmi les destinations possibles on dispose d'une pièce de monnaie, on ne peut donc que tirer à pile ou face. Proposez un moyen de simuler, avec la pièce de monnaie, le choix de la page à visiter si on se trouve sur chacune des 6 pages web.

✍️ **Question 8** : Utilisez le site <a href="http://pileouface.org/" target="_blank">http://pileouface.org/</a> pour tirer à pile ou face et parcourir 20 pages en tout selon les règles du jeu. Vous comptabiliserez le nombre de fois que chaque page a été visitée dans un tableau.

| Page | A | B | C | D | E | F |
| --- | --- | --- | --- | --- | --- | --- |
|Nombre de visites (perso) | | | | | | |

✍️ **Question 9** : Complétez le tableau partagé sur l'Espace Numérique de Travail avec vos valeurs pour mettre tous les résultats en commun. Nous calculerons le score de chaque page en faisant la somme des nombres de visites obtenus, scores que vous consignerez dans le tableau ci-dessous :

| Page | A | B | C | D | E | F |
| --- | --- | --- | --- | --- | --- | --- |
|Nombre de visites (classe) | | | | | | |

✍️ **Question 10** : Vérifiez que les pourcentages obtenus par la classe sont proches de :

| Page | A | B | C | D | E | F |
| --- | --- | --- | --- | --- | --- | --- |
| Pourcentage (score) | 15 % | 10 % | 10 % | 10 % | 40 % | 15 % |

<span class="emoji">✍️</span> **Question 5** : Quelle est la page la plus populaire ?

# Programmer PageRank avec Python

<img class="centre image-responsive" src="data/python_logo.svg" height="150">

Cette activité est à faire **sur Capytale** en utilisant le code ou le lien fourni par le professeur.

> Pour les élèves n'ayant pas accès à Capytale, il suffit de cliquer sur le lien suivant qui permet une ouverture avec Basthon : <a href="https://notebook.basthon.fr/?from=https://raw.githubusercontent.com/germainbecker/SNT/master/Capytale/T2_Web/Algorithme%20PageRank%20%5BELEVES%5D.ipynb" target="_blank">ouvrir avec Basthon</a>.




---
**Références**

- Manuel de SNT BORDAS (pages 57, 61 et 69) et cahier d'activités BORDAS (page 23)
- Conférence de Gerard Berry intitulée "Pourquoi l'informatique met le monde à l'envers" (2018, Université de Strasbourg): [https://youtu.be/0jqCyosZhFA](https://youtu.be/0jqCyosZhFA)
- Podcast *Le code a changé* (France Inter) intitulé "Le français qui a vu naître Google" (Serge Abiteboul) : [https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/le-francais-qui-a-vu-naitre-google-4402005](https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/le-francais-qui-a-vu-naitre-google-4402005)
- Graphe de la partie 3 créé avec [graph online](http://graphonline.ru/fr/?graph=vZaaBJOnrSKcaRYzZZcst)

---
Les enseignants de SNT du lycée Emmanuel Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)