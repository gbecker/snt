Données dans le nuage (cloud) : avantages et inconvénients
==========================================================

<img class="centre image-responsive" src="data/cloud.jpg" alt="illustration" width="400"/>
<p class="legende">Crédits : Photo de <a href="https://www.pexels.com/fr-fr/photo/vue-du-paysage-urbain-325185/" target="_blank">Aleksandar Pasaric</a> sur Pexels.</p>

<p class="description-seance" markdown="1">

Depuis l’apparition du cloud, on a tous un peu la tête dans les nuages... Le « cloud », cet anglicisme poétique, cache pourtant une réalité toute électronique qui a modifié notre quotidien et le monde de l'entreprise. Câbles, maintenance, data-centers, impacts sociétaux, enjeux écologiques, enjeux politiques, tout un menu !

</p>


# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :


*   Data centers et cloud :
    *   Définition du cloud (ou _cloud computing_, ou _informatique dans les nuages_)
    *   Rôle des data centers (ou _centres de données_, ou _data centres_, ou _centres informatiques_)
    *   Évolution du nombre de data centers, leur localisation, leur propriétaire
    *   Ordre de grandeur de la consommation énergétique des centres de données par rapport à celles des utilisateurs et des réseaux
*   Utilisation du cloud :
    *   Principaux services proposés par le cloud
    *   Évolution chiffrée de l’utilisation du cloud
    *   De nouvelles possibilités pour les particuliers (donner des exemples concrets) ...
    *   ... mais aussi pour les entreprises (donner des exemples concrets)
*   Inconvénients du cloud (points à détailler et illustrer par des exemples concrets) :
    *   Impact environnemental, consommation d’énergie
    *   Problèmes potentiels liés à la centralisation des données par des sociétés privées (américaines bien souvent) : enjeux de sécurité, enjeux politiques
    *   Autres inconvénients
*   Avantages du cloud (points à détailler et illustrer par des exemples concrets) :
    *   Disponibilité permanente
    *   Sécurité des données
    *   Réduction des coûts pour les entreprises
    *   Les progrès pour réduire la consommation d’énergie
    *   Attention à l'effet rebond

</div>

# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Articles Wikipédia : <a href="https://fr.wikipedia.org/wiki/Centre_de_donn%C3%A9es" target="_blank">Centre de données</a> et <a href="https://fr.wikipedia.org/wiki/Cloud_computing" target="_blank">Cloud computing</a>

🌍 Article du site <em>Le monde informatique</em> : <a href="https://www.lemondeinformatique.fr/actualites/lire-terminaux-datacenter-et-reseaux-la-pollution-numerique-en-chiffres-85511.html" target="_blank">Terminaux, datacenter et réseaux: la pollution numérique en chiffres</a>

🌍 Article du site <em>La Tribune</em> : <a href="https://www.latribune.fr/partenaires/accelerer-avec-le-cloud/le-cloud-pourrait-faire-economiser-80-d-energie-aux-entreprises-europeennes-904160.html" target="_blank">Le cloud pourrait faire économiser 80 % d’énergie aux entreprises européennes</a>

🌍 Études réalisées par le collectif <em>Green IT</em> (site : <a href="https://www.greenit.fr/" target="_blank">https://www.greenit.fr/</a>) : <a href="https://www.greenit.fr/wp-content/uploads/2019/10/2019-10-GREENIT-etude_EENM-rapport-accessible.VF_.pdf" target="_blank">Empreinte environnementale du numérique mondial</a> (septembre 2019, voir en priorité les chiffres de la partie 02) et <a href="https://www.greenit.fr/wp-content/uploads/2021/02/2021-01-iNum-etude-impacts-numerique-France-rapport-0.8.pdf" target="_blank">Impacts environnementaux du numérique en France</a> (janvier 2021, voir en priorité les chiffres des parties 2 et 3)

## Presse papier

📰 **Data centers : vos données sous haute surveillance**, article du magazine 01Net n°939 (20 octobre 2020), pages 38 et 39  
*👉 Disponible en PDF sur elyco et accessible sur Europresse via le MediaCentre d'elyco (puis onglet "Publications PDF" pour trouver le magazine).*

## Podcast

🎙️ **Le cloud : toutes et tous la tête dans les nuages ?** (20 min) : un épisode issu du podcast <a href="https://chut.media/podcasts-show/la-puce-a-loreille/" target="_blank">La Puce à l'oreille</a> de <a href="https://chut.media/podcasts/">Chut! Radio</a>, à écouter ci-dessous ou directement sur le site de <a href="https://chut.media/podcasts/la-puce-a-loreille-ep-12-le-cloud/" target="_blank">Chut! Radio</a>.

<iframe class="centre" name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-haDa" height="220" style="border: none; width:100%; height:220px" src="https://player.ausha.co/index.html?podcastId=oLY1RCewlqYE&v=3&playerId=ausha-haDa"></iframe>


## Vidéos

▶️ **Qu'est-ce que le cloud ?** - pixees Scienceparticipative, interview de Serge Abiteboul (7 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="5YawCCUxa_E" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/5YawCCUxa_E
</p>

▶️ **Datacenter : comprendre l'essentiel en 9 minutes (avec une belle visite en prime!)** - Cookie connecté (9 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="rO6bXt7d2L8" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/rO6bXt7d2L8
</p>

▶️ **Datacenters & pollution : ce qu'on ne vous dit PAS ! 🤔** - Underscore_ (29 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="cfK5EIRiTv4" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/cfK5EIRiTv4
</p>


# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

▶️ Dans les coulisses techniques de Netflix France - PP World (29 min) : <a href="https://youtu.be/4gGJJ6eNvU8" target="_blank">voir la vidéo</a>

▶️ Le cloud computing expliqué en 7 minutes - Cookie connecté (7 min) : <a href="https://youtu.be/RwbIMBSr8o8" target="_blank">voir la vidéo</a>

🎙️ Reportage <em>Dans le cloud en Islande, terre des data centers</em> issu de l'émission <em>De la terre au carré</em> de France Inter : <a href="https://www.radiofrance.fr/franceinter/podcasts/le-reportage-de-la-terre-au-carre/episode-1-dans-le-cloud-en-islande-terre-des-data-centers-5083548" target="_blank">épisode 1</a>, <a href="https://www.radiofrance.fr/franceinter/podcasts/le-reportage-de-la-terre-au-carre/episode-2-dans-le-cloud-en-islande-terre-des-data-centers-3977569" target="_blank">épisode 2</a>, <a href="https://www.radiofrance.fr/franceinter/podcasts/le-reportage-de-la-terre-au-carre/episode-3-dans-le-cloud-en-islande-terre-des-data-centers-5283928" target="_blank">épisode 3</a> et <a href="https://www.radiofrance.fr/franceinter/podcasts/le-reportage-de-la-terre-au-carre/episode-4-dans-le-cloud-en-islande-terre-des-datas-centers-5834396" target="_blank">épisode 4</a>

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


