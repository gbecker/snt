Abus d'écran : quelles sont les conséquences sur le cerveau, la santé et les comportements ?
============================================================================================


<img class="centre image-responsive" src="data/addiction_ecran.jpg" alt="illustration" width="400"/>
<p class="legende">Crédit : Photo de <a href="https://www.pexels.com/fr-fr/photo/gens-smartphone-connexion-addiction-8088495/">cottonbro studio</a> sur Pexels.</p>

<p class="description-seance" markdown="1">

Nous sommes bombardés quotidiennement par des dizaines, voire des centaines de notifications, d’« infos » et de vidéos qui ciblent parfaitement nos goûts et nos envies, tout en nous offrant l’illusion d’une consommation librement choisie. Mais, derrière les écrans, des algorithmes ont pris le contrôle de nos cerveaux. Ils ont été élaborés par une industrie numérique qui exploite une ressource extrêmement convoitée : *notre attention*. Les impacts et les conséquences de l'abus d'écran commencent à être connus, mais quels sont-ils ? Quelles sont les pistes pour une consommation plus équilibrée et profitable des écrans ?

</p>


# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :


* Des données statistiques
    * Du temps de présence devant écran en moyenne et selon les différentes catégories d'âge et catégories socio-professionnelles.
* Les dangers répertoriés sur l'abus des écrans concernant :
    * Le cerveau
    * La santé en général
    * Les comportements sociaux
* L'impact du temps des écrans sur l'économie et l’éducation :
    * Économie de l'attention
    * Apprentissage
* Les effets bénéfiques et pratiques d'une bonne gestion des écrans et du numérique :
    * Éducation
    * Économie, transport, écologie ?

</div>

# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Statistiques de l'INSEE : <a href="https://www.insee.fr/fr/statistiques/6535295?sommaire=6535307" target="_blank">Les enfants de moins de 6 ans et les écrans numériques</a>

🌍 Article du site <em>naitreetgrandir.com</em> : <a href="https://naitreetgrandir.com/fr/nouvelles/2022/12/14/temps-ecran-enfants-pediatres-maintiennent-recommandations/" target="_blank">Temps d'écran des enfants: les pédiatres maintiennent leurs recommandations</a>

🌍 Dernière partie de la séance 1 sur les réseaux sociaux sur <em>info-mounier.fr</em> : <a href="https://info-mounier.fr/snt/reseaux_sociaux/reseaux-existants.php#l%C3%A9conomie-de-lattention" target="_blank">L'économie de l'attention</a> (il y a aussi des vidéos)

🌍 Article du site <em>Science & Vie</em> : <a href="https://www.science-et-vie.com/corps-et-sante/la-violence-televisuelle-avant-3-ans-peut-causer-des-troubles-psychologiques-jusqua-ladolescence-94502.html" target="_blank">La violence télévisuelle avant 3 ans ...</a>

🌍 Interview de Michel Desmurget dans le magazine <em>Usbek & Rica</em> (25 septembre 2023) : <a href="https://usbeketrica.com/fr/article/michel-desmurget-ce-n-est-pas-un-hasard-si-les-livres-et-le-langage-ont-de-tous-temps-ete-la-cible-obsessionnelle-des-tyrannies" target="_blank">« Nos enfants ne lisent plus »</a>

## Presse papier

📰 **Il n'y a pas d'addictions aux écrans (Wendy Wood)**, article du magazine Epsiloon n°1 (juillet 2021), rubrique "Contrepied" page 30  
*👉 Disponible au CDI*

📰 **Prisonniers de l’infini : enquête sur le piège des écrans** (comment le scroll piège notre cerveau ?), article du magazine Epsiloon HS n°3 (juillet 2022), page 42.    
*👉 Disponible au CDI*

## Podcast

🎙️ **Sommes-nous vraiment en train de fabriquer des “crétins digitaux" ?** (47 min) : un épisode (9 novembre 2020) issu du podcast *Le code a changé* de Xavier de la Porte, à écouter ci-dessous ou directement sur le site de <a href="https://www.radiofrance.fr/franceinter/podcasts/le-code-a-change/sommes-nous-vraiment-en-train-de-fabriquer-des-cretins-digitaux-5461330" target="_blank">France Inter</a>. 

<div class="podcast-container">
    <img class="image-responsive centre" src="https://www.radiofrance.fr/s3/cruiser-production/2022/07/f560e31f-8209-4902-8195-52cd9769eb1e/1400x1400_rf_omm_0000024370_ite.jpg" alt="icone podcast" width="200">
    <audio
        class="centre"
        controls
        preload="metadata"
        src="https://rf.proxycast.org/b00ce642-8fb9-4cf9-be1f-328c92971425/20856-09.11.2020-ITEMA_22478626-2020F38589E0016-21.mp3">
    </audio>
</div>

## Vidéos

▶️ **Des écrans malfaisants, vraiment** - Lumni (4 min)  
*👉 Pour voir cette vidéo : le lien direct est dans l'espace de travail sur elyco ou aller dans le Mediacentre d'elyco puis cliquer sur "Lumni enseignement" et tapez dans la barre de recherche le nom de la vidéo : "Des écrans malfaisants, vraiment" et cliquer sur le premier résultat.*

▶️ **Écrans : le vrai du faux sur la santé des enfants par Michel Desmurget, chercheur en neurosciences** - Le Point (3 min)

<div class="video-responsive">
    <div class="dailymotion_player centre" videoID="x7fxiyb" width="560" height="315" showinfo="1" autoplay="0" embedType="video"></div>
</div>
<p class="impression">
    Source vidéo : https://dai.ly/x7fxiyb
</p>

▶️ **L'économie de l'attention : interview de Bruno Patino et Léo Favier** - Canopé (7 min)

<div class="video-responsive">
    <video class="centre" width="560" height="315" controls name="media"><source src="https://cdn.reseau-canope.fr/medias/dopamine/dopamine_itw_economie_attention-360p.mp4" type="video/mp4"></video>
</div>
<p class="legende effacer-impression">Source : Dossier Canopé sur <a href="https://www.reseau-canope.fr/la-course-a-lattention.html">La course à l'attention</a>.</p>
<p class="impression">
    Source : Dossier Canopé sur <a href="https://www.reseau-canope.fr/la-course-a-lattention.html">La course à l'attention</a>.
</p>

▶️ **Écran total, le cerveau hyperconnecté** - France 5 (52 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="st1iRyaAXvU" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/st1iRyaAXvU
</p>


# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

🌍 Dossier Canopé sur la course à l'attention : <a href="https://www.reseau-canope.fr/la-course-a-lattention.html" target="_blank">voir le dossier</a>.

▶️ Mini série <em>Dopamine</em> de courtes vidéos : <a href="https://www.arte.tv/fr/videos/RC-017841/dopamine/" target="_blank">voir les épisodes sur arte.tv</a>

📰 **Réseaux sociaux : la vraie vie ?**, article du magazine Science & Vie Junior HS n°157 (janvier 2023), pages 44-47.  
*👉 Disponible en PDF sur elyco et accessible sur Europresse via le MediaCentre d'elyco (puis onglet "Publications PDF" pour trouver le magazine).*

🌍 *Quels sont les impacts des écrans sur les cerveaux ?* : <a href="https://www.strategie.gouv.fr/debats/impacts-ecrans-cerveaux" target="_blank">écouter le podcast</a> sur strategie.gouv.fr  
*Une synthèse écrite est disponible sous le podcast.*

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


