Sport et data : une révolution ?
================================

<img class="centre image-responsive" src="data/sport_et_data_2.png" alt="illustration : recyclage d'un smartphone" width="300">
<p class="legende">
    Crédits : Image créée par l'IA Dall-E.
    <br><span class="hovertext" data-hover="Create a futuristic image of a contemporary sports scene with athletes participating in various sports, surrounded by modern digital data collection equipment, highlighting the intersection of sports and advanced data technology">Voir le prompt</span>.
</p>

<p class="description" markdown="1">

Les pratiques sportives sont aujourd'hui bouleversées par la collecte et l'analyse de données, aussi bien dans le sport professionnel qui fait désormais appel à des "coachs data", que pour les sportifs amateurs équipés d'applications ou autres montres connectées. Comment les données impactent-elles le sport ? Quels avantages offrent-elles aux athlètes, aux coachs et aux amateurs ? Outre les opportunités offertes par les données, quels sont les dangers potentiels et les défis éthiques qu'elles posent ?

</p>

# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :

- L'utilisation des données dans le sport professionnel
    - Exemples concrets de données collectées
    - Technologies et méthodes pour collecter les données
    - Objectifs et impacts sur les pratiques et la performance des athlètes
- La démocratisation des données pour les sportifs amateurs
    - Exemples concrets d'accès aux données pour les amateurs (montres, applications, etc.)
    - Impact sur leurs pratiques sportives
- Les menaces et dangers potentiels (donner des exemples concrets)
    - biais statistiques et éthique dans la collecte et l'utilisation des données
    - dépendance aux données et protection des données personnelles
    - surveillance
    - etc.

</div>


# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne 

🌍 Article du site *The Conversation* : <a href="https://theconversation.com/football-quand-les-donnees-se-mettent-au-service-de-la-performance-199318" target="_blank">Football : quand les données se mettent au service de la performance</a>

🌍 Article du site *lebigdata.fr* : <a href="https://www.lebigdata.fr/sport-et-big-data" target="_blank">Sport et Big Data – Quand la science des données donne l’avantage sur le terrain</a>

🌍 Article du site *peppy.cool* : <a href="https://peppy.cool/blog/clk9fdty814314751hn58h2w8pta/comment-la-data-modifie-la-pratique-sportive/" target="_blank">Comment la data modifie la pratique sportive ?</a>

🌍 Article du Mag de l'INSEP (novembre-décembre 2022, **pages 10 à 15**) : <a href="https://www.insep.fr/sites/default/files/media/downloads/INSEP_lemag_52_nov_dec_2022-light.pdf" target="_blank">Le Sport Data Hub, un nouvel élément clé pour le sport français</a>

## Presse papier

📰 **Quand la Tech se met au service du sport**, article du magazine Chut! n°14 (octobre, novembre, décembre 2023), pages 44-47.

<a class="effacer-impression" href="https://chut.media/tech/quand-la-tech-se-met-au-sport/" target="_blank"><img class="centre image-responsive" src="https://chut.media/wp-content/uploads/2023/09/chut-14_mag-couv-796x1024.jpg" width="220"></a>

*👉 Article en ligne réservé aux abonnés mais disponible dans sa version complète en PDF sur elyco.*

## Podcasts

🎙️ **JO, Rugby... : Comment la data transforme le sport de haut niveau** (59 min) : un épisode (1 septembre 2023) issu du podcast *Le Meilleur des Mondes*, à écouter ci-dessous ou directement sur le site de <a href="https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/jo-rugby-comment-la-data-transforme-le-sport-de-haut-niveau-4681690" target="_blank">Radio France</a>.

<details class="deroulement-simple effacer-impression" markdown="1">

<summary>Voir le pitch de l'émission</summary>

Alors que la coupe du Monde de Rugby débute dans une semaine exactement, le 8 septembre 2023, que les JO 2024 se dessinent à l’horizon, Le Meilleur des Mondes consacre sa première émission aux mutations du sport de haut niveau sous l’impulsion de la data et des nouvelles technologies. Comment juger l'efficacité de ces nouveaux outils ? Quelles peuvent en être les dérives ? Que reste-t-il de la puissance, de l'intuition ou de la beauté du sport lorsque sa pratique semble réduite à des graphes et des chiffres ?

Des questions auxquelles tenteront de répondre nos quatre invités :

- **Yannick Nyanga**, entraîneur Espoirs du Racing 92, ancien joueur international de rugby et co-auteur de Data et sport, la Révolution
- **Aurélie Jean**, docteure en sciences et entrepreneure, spécialiste des algorithmes et co-autrice de Data et sport, la Révolution
- **Nicolas Benguigui**, enseignant-chercheur en STAPS dans le domaine des sciences cognitives et sciences des numériques au laboratoire GREYC de l’Université de Caen
- **Chloé Gobé**, data scientist à l'INSEP, chargée du projet Médaillabilité. 

</details>

<div class="podcast-container effacer-impression">
    <img class="image-responsive centre" src="https://www.radiofrance.fr/s3/cruiser-production/2021/09/fa120aea-8f77-4479-87f2-5791e18c185e/1400x1400_le-meilleur-des-mondes.jpg" alt="icone podcast" width="200">
    <audio
        class="centre"
        controls
        preload="metadata"
        src="https://rf.proxycast.org/b862b514-0b7b-4746-a05a-fce83639cb6d/22826-01.09.2023-ITEMA_23473010-2023C45296E0002-21.mp3">
    </audio>
</div>


🎙️ **Sport et data : après quoi court-on ?** (59 min) : un épisode (18 février 2022) issu du podcast *Le Meilleur des Mondes*, à écouter ci-dessous ou directement sur le site de <a href="https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/sport-et-data-apres-quoi-court-on-4065106" target="_blank">Radio France</a>.

<details class="deroulement-simple effacer-impression" markdown="1">

<summary>Voir le pitch de l'émission</summary>

Et ce soir on chausse les baskets, on monte en selle et on plonge dans nos pratiques sportives qui sont de plus en plus connectées. Montres intelligentes, bracelets, smartphones vissés au bras, on court vers la data. On scrute nos performances pour mieux les comparer avec celles du voisin, on analyse notre rythme cardiaque tout en écoutant parfois les conseils santé d’une Intelligence artificielle.

Que disent ces usages de notre société ? Comment s’intègrent-ils dans l’histoire du sport et comment ces données livrées à la sueur de notre front servent-elles les intérêts du marketing ? Quelles sont les dérives possibles de cette surveillance de nos activités sportives ?

Pour en discuter, nous recevons :

- **Paul-Emile Saab**, CEO de Sport Heroes et co-président de la French SporTech,
- **Raphaël Verchère**, docteur en philosophie et triathlète, auteur du livre "Philosophie du triathlon" (Ed. Du Volcan, 2020),
- **Bastien Soulé**, sociologue, professeur des universités à l’Université Claude Bernard Lyon 1, chercheur au sein du Laboratoire sur les Vulnérabilités et l’Innovation dans le Sport (L-ViS).

</details>

<div class="podcast-container effacer-impression">
    <img class="image-responsive centre" src="https://www.radiofrance.fr/s3/cruiser-production/2021/09/fa120aea-8f77-4479-87f2-5791e18c185e/1400x1400_le-meilleur-des-mondes.jpg" alt="icone podcast" width="200">
    <audio
        class="centre"
        controls
        preload="metadata"
        src="https://rf.proxycast.org/b862b514-0b7b-4746-a05a-fce83639cb6d/22826-01.09.2023-ITEMA_23473010-2023C45296E0002-21.mp3">
    </audio>
</div>

## Vidéos

▶️ **SPORT DATA HUB** - Agence nationale du Sport (3 min)

<div class="video-responsive">
    <div class="vimeo_player centre" videoID="515661695" width="560" height="315"></div>
</div>
<p class="impression">
    Source vidéo : https://vimeo.com/515661695
</p>

▶️ **Data & Sport : histoire d’une révolution sportive** - C à Vous - 06/09/2023 (5 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="oil5MlNsgBA" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/oil5MlNsgBA
</p>
    
▶️ **La data au service du foot** - CentraleSupelec (6 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="sPHD78Us_WE" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/sPHD78Us_WE
</p>

▶️ **La data fait-elle la loi ?** - Sport Unlimitech (41 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="u6U97z0P_KA" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/u6U97z0P_KA
</p>




# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

▶️ **La Data au cœur de la performance : analyse, protection, enjeux et perspectives**, débat sur la chaîne *Sport Unlimitech* (41 min) : <a href="https://youtu.be/XOlkwcYjJFc" target="_blank">https://youtu.be/XOlkwcYjJFc</a>.

🌍 Article du magazine *Usbek & Rica*, en partenariat avec Adidas (10 septembre 2021) : <a href="https://usbeketrica.com/fr/article/dans-le-sport-la-data-ne-sert-pas-qu-a-battre-des-records" target="_blank">Dans le sport, la data ne sert pas qu’à battre des records</a>.

🌍 Témoignage de Geoffroy Berhtelot, data scientist à l'INSEP : <a href="https://ensai.fr/data-science-sport-geoffroy-berthelot/" target="_blank">Data science et sport : défis et perspectives</a>.

🎙️ Épisode en podcast de la chronique *Un monde connecté*, de François Saltiel sur France Culture : <a href="https://www.radiofrance.fr/franceculture/podcasts/un-monde-connecte/sportifs-professionnels-data-un-usage-tout-terrain-3828638" target="_blank">Sportifs professionnels et data, un usage tout terrain</a>.
<br>

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


