La révoltion ChatGPT
====================

<img class="centre image-responsive" src="data/ChatGPT_logo.svg" alt="illustration" width="250"/>
<p class="legende">Crédit : <a href="https://commons.wikimedia.org/wiki/File:ChatGPT_logo.svg">ChatGPT</a>, Domaine public, via Wikimedia Commons.</p>

<p class="description-seance" markdown="1">

ChatGPT, tout le monde en parle depuis fin 2022. L'accès au grand public de cet agent conversationnel, qui semble avoir réponse à tout, marque un nouveau pas dans les progrès de l'intelligence artificielle (IA) et pose énormément de questions. Mais qu'est-ce que réellement ChatGPT ? Comment fonctionne cette IA ? Comment peut-on l'utiliser intelligemment ? Faut-il s'en méfier ? Autant de questions sur lesquelles se pencher pour cet exposé !

</p>

# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :


- ChatGPT : qu'est-ce que c'est ?
    - Présentation de cette IA
    - Société à l’origine de ChatGPT
    - Le fonctionnement de ChatGPT : données d’entraînement, entraînement assisté par les humains, le modèle prédit des mots
- Analyse des réponses de ChatGPT
    - Peut-il répondre à tout ?
    - Des réponses parfois impressionnantes… mais parfois complètement fausses (trouver des exemples, tester soi-même)
    - Les limites et les biais des réponses produites
- Les possibilités offertes par ChatGPT
    - Exemples d’utilisations possibles dans différents secteurs
    - Aller au-delà de l'outil de triche : exemples d'utilisations intelligentes par les élèves
    - Impact sur les métiers
- Les dangers potentiels de ChatGPT
    - Les questions d’éthique
    - Des possibles manipulations, fausses informations
    - La porte ouverte à la cybercriminalité ?

</div>


# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Article du site <em>lebigdata.fr</em> Statistiques de l'INSEE : <a href="https://www.lebigdata.fr/chatgpt" target="_blank">ChatGPT, le guide complet : pourquoi cette IA va changer le monde</a>

🌍 Article du site <em>lemonde.fr</em> dans sa rubrique <em>Pixels</em> : <a href="https://www.lemonde.fr/pixels/article/2023/02/12/chatgpt-on-a-teste-les-limites-morales-de-l-intelligence-artificielle-generatrice-de-contenus_6161487_4408996.html" target="_blank">ChatGPT : on a testé les limites morales de l’intelligence artificielle génératrice de contenus</a>   
👉 *Version en ligne réservée aux abonnés, mais article complet disponible en PDF sur elyco.*

🌍 Article du site <em>eductive.ca</em> sur des utilisations possibles à l'école : <a href="https://eductive.ca/ressource/chatgpt-lintelligence-artificielle-aux-portes-des-etablissements-du-reseau-collegial/" target="_blank">ChatGPT : L'intelligence artificielle aux portes des établissements du réseau collégial</a>

## Presse papier

📰 **ChatGPT, ce n'est que le début**, dossier du magazine Epsiloon n°25 (juillet 2023), pages 43 à 57  
👉 *Disponible au CDI.*

<a href="https://www.epsiloon.com/common/product-article/140" target="_blank">
    <img class="centre image-responsive" src="https://static.aboshop.fr/downloadservice/aboshop/epsiloon/products/140/covers/1.jpg?v=20230904122253" alt="couverture du magazine" width="200">
</a>

📰 **ChatGPT à l'école : et si c'était bien ?**, article du magazine Télérama n°3816 (4 mars 2023), pages 20-24  
👉 *Disponible en PDF sur elyco et accessible sur Europresse via le MediaCentre d'elyco (puis onglet "Publications PDF" pour trouver le journal).*

## Podcast

🎙️ **ChatGPT : les nouveaux enjeux de l’IA** (59 min) : un épisode (20 janvier 2023) issu du podcast *Le Meilleur des mondes*, à écouter ci-dessous ou directement sur le site de <a href="https://www.radiofrance.fr/franceculture/podcasts/le-meilleur-des-mondes/chat-gpt-les-nouveaux-enjeux-de-l-i-a-8203254" target="_blank">Radio France</a>. 

<details class="deroulement-simple effacer-impression" markdown="1">

<summary>Voir le pitch de l'émission</summary>

Ce soir nous allons tenter d'ouvrir la machine ChatGPT pour mieux comprendre ses enjeux techniques, économiques et éthiques. ChatGPT, un agent conversationnel qui utilise l'intelligence artificielle pour répondre à toutes vos questions en un clic et qui parvient à simuler l'écriture humaine. Cette technologie fascine, surprend, inquiète et fait l'actualité depuis plusieurs semaines.

Mais s'agit-il réellement d'une innovation de rupture ? Qui se cache derrière OpenAI, l'organisation à l'origine de ce programme ? Pourquoi Microsoft compte investir 10 milliards de dollars dans cette machine pour mieux la décliner sur nos mails, nos moteurs de recherche. Et comment les autres GAFAM s'apprêtent-ils à riposter ? Quel est le modèle économique et ces dérives ? Enfin, comment ne pas s'inquiéter des potentiels dangers d'un tel outil dans les mains de cybercriminels ou de spécialités de la manipulation ?

Beaucoup de questions et trois invités pour y répondre :

- **Chloé Clavel**, Professeure associée en informatique affective à Telecom ParisTech
- **Lê Nguyên Hoang**, Mathématicien, vidéaste, créateur de la chaîne YouTube *Science4All*
- **Tariq Krim**, Entrepreneur, fondateur de Netvibes, Jolicloud et de la plateformes de web éthique Polite. Ancien vice-président du Conseil du numérique, spécialiste des questions d'éthique et de vie privée sur Internet. 

</details>

<div class="podcast-container effacer-impression">
    <img class="image-responsive centre" src="https://www.radiofrance.fr/s3/cruiser-production/2021/09/fa120aea-8f77-4479-87f2-5791e18c185e/1400x1400_le-meilleur-des-mondes.jpg" alt="icone podcast" width="200">
    <audio
        class="centre"
        controls
        preload="metadata"
        src="https://media.radiofrance-podcast.net/podcast09/22826-02.06.2023-ITEMA_23398260-2023C45296S0153-21.mp3?podcast=podcast09/22826-02.06.2023-ITEMA_23398260-2023C45296S0153-21.mp3&geoipcountry=FR&provider=public&br=45296&stationname=France+Culture">
    </audio>
</div>

## Vidéos

▶️ **Comment fonctionne ChatGPT ?** - INRIA Flowers (5 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="K8gOvC8gvB4" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/K8gOvC8gvB4
</p>

▶️ **Le prompting, ou l’art de se faire comprendre par ChatGPT** - INRIA Flowers (5 min) 

<div class="video-responsive">
    <div class="youtube_player centre" videoID="8IQ9i_QoA3A" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/8IQ9i_QoA3A
</p>

▶️ **Quelles sont les limites de ChatGPT ?** - INRIA Flowers (5 min) 

<div class="video-responsive">
    <div class="youtube_player centre" videoID="xXHWTC4mJBM" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/xXHWTC4mJBM
</p>

▶️ **Pourquoi ChatGPT est une révolution (qui inquiète)** - HugoDécrypte (21 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="OG2Y_R6fvvo" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/OG2Y_R6fvvo
</p>

▶️ **La face (très) sombre de ChatGPT ...** - Defend Intelligence (10 min)
          
<div class="video-responsive">
    <div class="youtube_player centre" videoID="QEYBMXZG6yY" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/QEYBMXZG6yY
</p>

# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

<blockquote class="information" markdown="1">

Si vous devez regardez une de ces ressources supplémentaires, c'est bien la première vidéo de David Louapre, qui explique le fonctionnement de ChatGPT.

</blockquote>


▶️ **Ce qui se cache derrière le fonctionnement de ChatGPT** de la chaîne *ScienceEtonnante* (27 min) : <a href="https://youtu.be/7ell8KEbhJo" target="_blank">https://youtu.be/7ell8KEbhJo</a>  

▶️ **Comment les I.A. font-elles pour comprendre notre langue ?** de la chaîne *ScienceEtonnante* (26 min) : <a href="https://youtu.be/CsQNF9s78Nc" target="_blank">https://youtu.be/CsQNF9s78Nc</a>  

▶️ **L'histoire chaotique de l'Intelligence Artificielle**, de la chaîne *cocadmin* (18 min) : <a href="https://youtu.be/RKtR-lSOq2g" target="_blank">https://youtu.be/RKtR-lSOq2g</a>  

🎙️ Épisode 41 du **podcast *Complorama*** de France Info : <a href="https://www.radiofrance.fr/franceinfo/podcasts/complorama/chatgpt-l-intelligence-artificielle-au-service-du-complotisme-1527533" target="_blank">ChatGPT : l'intelligence artificielle au service du complotisme ?</a>

📰 **7 questions sur ChatGPT**, article du magazine 01Net n°994 (15 mars 2023), pages 22-25  
👉 *Disponible en PDF sur elyco et accessible sur Europresse via le MediaCentre d'elyco (puis onglet "Publications PDF" pour trouver le journal).*

📰 **La folie ChatGPT**, dossier du *Courrier International* n°1685 du 16 février 2023, pages 28-35.  
*👉 Dossier complet disponible en PDF sur elyco.*

📰 **L'IA, un atout pour les métiers créatifs**, article du journal *Les Échos* du 3 janvier 2023, page 24.  
*👉 Version <a href="hhttps://www.lesechos.fr/tech-medias/hightech/lia-un-outil-deja-indispensable-pour-les-metiers-creatifs-1893255" target="_blank">en ligne</a> réservée aux abonnées mais disponible en PDF sur elyco et accessible sur Europresse via le MediaCentre d'elyco (puis onglet "Publications PDF" pour trouver le journal).*

📰 **Comprendre comment fonctionne ChatGPT avant de l’utiliser**, tribune de Laurence Devillers dans le journal *Le Monde* du 22-23 janvier 2023, page 28.  
*👉 Version <a href="https://www.lemonde.fr/idees/article/2023/01/20/chatgpt-saluer-l-avancee-technologique-mais-comprendre-les-limites-de-ce-type-de-systeme_6158602_3232.html" target="_blank">en ligne</a> réservée aux abonnées, mais disponible en PDF sur elyco et accessible sur Europresse via le MediaCentre d'elyco (puis onglet "Publications PDF" pour trouver le journal).*

🌍 Article du journal <em>Les Échos</em> : <a href="https://www.lesechos.fr/tech-medias/hightech/chatgpt-plongee-dans-les-entrailles-du-chatbot-qui-bouleverse-la-tech-1888443" target="_blank">ChatGPT : plongée dans les entrailles du chatbot qui bouleverse la tech</a>  
👉 *Aussi disponible en PDF sur elyco.*

🌍 Article du jounal <em>Marianne</em> : <a href="https://www.marianne.net/agora/tribunes-libres/chatgpt-sortons-du-triptyque-les-americains-innovent-les-chinois-copient-et-les-europeens-reglementent" target="_blank">ChatGPT : "Sortons du triptyque : les Américains innovent, les Chinois copient et les Européens réglementent"</a>


<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


