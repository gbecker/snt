Les cookies nous surveillent-ils ?
==================================

<img class="centre image-responsive" src="data/cookies.jpg" alt="illustration" width="300"/>
<p class="legende">Crédit : <a href="https://pixabay.com/fr/photos/biscuits-smartphone-6613491/">nicki_kng</a>, via Pixabay.</p>

<p class="description-seance" markdown="1">

Depuis quelques années, notre navigation sur le Web est rythmée par des cookies : on peut les accepter, les refuser, les ignorer et même s'en agacer ! Mais au fait, qu'est-ce qu'un cookie ? Pourquoi certains sont nécessaires alors que d'autres posent des soucis en terme de confidentialité ? Comment régler son navigateur pour gérer correctement les cookies ?

</p>


# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :

- Qu'est-ce qu'un cookie ?
    - Définition
    - Les différents types de cookies
- Certains cookies sont indispensables au fonctionnement de certains sites
    - Donner des exemples concrets
- Les cookies utilisés pour traquer les internautes
    - Donner des exemples concrets
    - Les dangers et les dérives liés à la confidentialité
- Gérer les cookies, la sécurité et la confidentialité dans son navigateur
    - Expliquer comment voir les cookies stockés par son navigateur
    - Expliquer comment supprimer des cookies de son navigateur
    - Donner des bonnes pratiques pour mieux se protéger

</div>

# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Ressource de la CNIL : <a href="https://www.cnil.fr/fr/definition/cookie" target="_blank">Définition d'un cookie</a>

🌍 Ressource de la CNIL : <a href="https://www.cnil.fr/fr/cookies-et-autres-traceurs" target="_blank">Site web, cookies et autres traceurs</a>

🌍 Ressource de la CNIL : <a href="https://www.numerama.com/tech/701279-payer-pour-eviter-les-cookies-publicitaires-est-ce-legal.html" target="_blank">Cookie walls</a>

🌍 Article de NUMERAMA : <a href="https://www.arcep.fr/fileadmin/cru-1658411991/user_upload/arcep/nos-sujets/cartographie-net-neutralite-2018_conf050618.pdf" target="_blank">Payer pour éviter les cookies publicitaires, est-ce légal ?</a>

🌍 Article de L'INTERNAUTE : <a href="https://www.linternaute.fr/hightech/guide-high-tech/1413150-comment-supprimer-les-cookies-facilement/" target="_blank">Comment supprimer les cookies facilement</a>

## Vidéos

▶️ **Qu’est-ce qu'un cookie ?** - CNIL (1 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="a9kiQPV_SPg" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/a9kiQPV_SPg
</p>

▶️ **CONCRETEMENT, C'EST QUOI UN COOKIE ?** - MisterHoodie (3 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="HfiJ3ME8Tvs" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/HfiJ3ME8Tvs
</p>

▶️ **TUTORIEL | Qu'est-ce qu'un cookie ?** - CNIL ! (3 min)

<div class="video-responsive">
    <div class="dailymotion_player centre" videoID="x16lt53" width="560" height="315" showinfo="1" autoplay="0" embedType="video"></div>
</div>
<p class="impression">
    Source vidéo : https://dai.ly/x16lt53
</p>

▶️ **LES COOKIES VOUS SURVEILLENT !** - Shubham Sharma (7 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="fm5MSdPU8tY" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/fm5MSdPU8tY
</p>

▶️ **TUTORIEL | Effacer les traces de sa navigation** - CNIL (4 min)

<div class="video-responsive">
    <div class="dailymotion_player centre" videoID="xw48jr" width="560" height="315" showinfo="1" autoplay="0" embedType="video"></div>
</div>
<p class="impression">
    Source vidéo : https://dai.ly/xw48jr
</p>

# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

▶️ Vidéo de la chaîne KPulse : *Qu’est-ce qu’un cookie ? (web)* (3 min) : <a href="https://youtu.be/iEa5iGZTGLU" target="_blank">voir la vidéo</a>

🌍 Article du site Cookiebot.com : <a href="https://www.cookiebot.com/fr/cookies-internet/" target="_blank">Cookies Internet | Que sont-ils et que font-ils ?</a>

🌍 Article du site digitiz.fr : <a href="https://digitiz.fr/blog/cookies-internet/" target="_blank">Cookies Internet : C’est quoi et quels sont leurs utilités ?</a>


<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


