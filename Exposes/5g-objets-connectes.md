La 5G et les objets connectés : intérêts et risques
===================================================

<img class="centre image-responsive" src="data/5G.jpg" alt="illustration" width="400"/>
<p class="legende">Crédits : <a href="https://pixabay.com/fr/illustrations/la-technologie-5g-air-abstrait-4816658/">mohamed_hassan</a>, via Pixabay.</p>

<p class="description-seance" markdown="1">

La 5G est accessible depuis quelques années dans les grandes villes mais cette technologie a suscité et suscite encore bon nombre de débats scientifiques, économiques, politiques, géopolitiques et écologiques. Mais qu'est-ce que réellement la 5G ? Comment favorise-t-elle l'utilisation d'objets connectés ? Quelles sont les possibilités offertes par cette technologie et quels sont les risques associés ?

</p>


# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :

*   Qu’est-ce que la 5G ?
    *   Définition
    *   Différence avec la 4G
    *   Déploiement en France
*   Les objets connectés c'est quoi ?
    *   Définition
    *   Exemples industriels et domestiques
*   Utilisations et intérêts de la 5G avec les objets connectés
*   Risques et impacts négatifs de la 5G et des objets connectés
*   Mesures pour amélioration la sécurité de la 5G et des objets connectés

</div>

# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Article Wikipédia : <a href="https://fr.wikipedia.org/wiki/5G" target="_blank">La 5G</a>

🌍 Article du site *Thalès* : <a href="https://www.thalesgroup.com/fr/europe/france/dis/mobile/inspiration/5g" target="_blank">Qu’est-ce que la 5G ?</a>

🌍 Article du site *The Conversation* : <a href="https://theconversation.com/quelles-seront-les-applications-concretes-de-la-5g-170843" target="_blank">Quelles seront les applications concrètes de la 5G ?</a>

🌍 Article du site *Comment ça Marche* : <a href="https://www.commentcamarche.net/securite/confidentialite/25029-privacy-not-included-la-liste-d-objets-connectes-a-eviter/" target="_blank">Privacy not included : le guide d'objets connectés à éviter</a> à propos du site <a href="https://foundation.mozilla.org/fr/privacynotincluded/" target="_blank">Privacy not included</a> de la *Mozilla Foundation* qui recense tous les objets connectés que ne respectent pas la vie privée des utilisateurs.

🌍 Article du site *acteurspublics.fr* : <a href="https://acteurspublics.fr/articles/letat-veut-en-savoir-plus-sur-limpact-des-objets-connectes" target="_blank">L’État veut en savoir plus sur l’impact des objets connectés</a>.  
*👉 Version en ligne réservée aux abonnés mais article complet disponible en PDF sur elyco*

## Presse papier

📰 **Tribune** de *Serge Abiteboul* parue dans *L'Humanité magazine* (19-24 mai 2022, pages 42-43) dans le cadre d'un partenariat avec l'Académie des sciences : <a href="https://www.academie-sciences.fr/pdf/revue/LHUMANITE_mai2022.pdf" target="_blank">La 5G, une rupture et des questionnements</a>.

📰 **Tribune** de *Serge Abiteboul et Gérard Berry* parue dans le journal économique *La Tribune* (n°7260, 3 nov. 2021, pages 92-95) aussi disponible en ligne : <a href="https://www.latribune.fr/opinions/tribunes/environnement-securite-sante-les-questions-que-pose-la-5g-894951.html" target="_blank">Environnement, sécurité, santé : les questions que pose la 5G</a>.

## Vidéos

▶️ **La 5G, c’est quoi?** - Radio-Canada Info (3 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="Cj5ZN6pYxBM" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/Cj5ZN6pYxBM
</p>

▶️ **3 minutes pour comprendre... les objets connectés (IoT)** - WAYA TECH (3 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="qPh8N01447E" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/qPh8N01447E
</p>

▶️ **Objets connectés : quels sont les risques ?** - Conso Mag (2 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="NKDCGxOevqM" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/NKDCGxOevqM
</p>

▶️ **IoT : L'internet des objets expliqué en emoji** - Cookie connecté (4 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="8sgmHTHSqxw" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/8sgmHTHSqxw
</p>

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


