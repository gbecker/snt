Internet : par où passent les données ?
=======================================

<img class="centre image-responsive" src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Submarine_cable_map_umap.png/800px-Submarine_cable_map_umap.png" alt="illustration" width="400"/>
<p class="legende">Crédit : <a href="https://commons.wikimedia.org/wiki/File:Submarine_cable_map_umap.png">Greg Mahlknecht</a>, <a href="https://creativecommons.org/licenses/by-sa/2.0">CC BY-SA 2.0</a>, via Wikimedia Commons.</p>

<p class="description-seance" markdown="1">

On peut accéder à Internet de manière totalement transparente quel que soit le type de réseau physique utilisé (fibre, wi-fi, etc), on dit qu'Internet est indépendant du réseau physique. Les données qui transitent sur Internet sont de plus en plus volumineuses, mais par où passent-elles ? Qui possède les infrastructures nécessaires ? Quels sont les enjeux (géo)politiques ?

</p>

# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :

*   Les différents types de réseaux physiques et les caractériser : obsolètes ou actuels, rapides ou lents, filaire ou non.
*   L'évolution de l'ordre de grandeur du trafic de données sur Internet :
    *   Chiffres intéressants
    *   Répartition du trafic selon le type de données
*   L'importance des câbles sous-marins dans le réseau Internet :
    *   Technologie des câbles
    *   Rôle, géographie, nombre et propriétaires des câbles
    *   Enjeux des câbles sous-marins (donner des exemples intéressants et concrets) :
        *   Enjeux d'exploitation : les GAFAM dans la bataille, fin de la neutralité du Net ?
        *   Enjeux de souveraineté
        *   Enjeux politiques et géopolitiques

</div>

# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Carte interactive des câbles sous-marins : <a href="https://www.submarinecablemap.com/" target="_blank">https://www.submarinecablemap.com/</a>

🌍 Article du Blog du modérateur (BDM) : <a href="https://www.blogdumoderateur.com/rapport-arcep-internet-france-2022/" target="_blank">Rapport de l’ARCEP : l’état d’Internet en France en 2022</a>

🌍 Article du magazine *Usbek & Rica* du 14 mai 2023 : <a href="https://usbeketrica.com/fr/article/la-moitie-de-la-capacite-des-cables-mondiaux-est-aujourd-hui-utilisee-par-les-gafam" target="_blank">La moitié de la capacité des câbles mondiaux est aujourd’hui utilisée par les GAFAM</a>

🌍 Article Numérama : <a href="https://www.numerama.com/tech/1026270-netflix-continue-decraser-le-trafic-internet-en-france.html" target="_blank">Netflix continue d’écraser le trafic Internet en France</a>

🌍 Graphique sur statista.com : <a href="https://fr.statista.com/infographie/15784/projets-de-cables-de-telecommunications-sous-marins-realises-par-les-gafam/">Câbles sous-marins : les géants de la tech tissent leur toile</a>


## Presse papier

📰 Article **"Ces câbles qui nous relient à Internet"** du magazine 01Net (n°976 du 29 juin 2022, pages 34-35)  
👉 *Disponible sur elyco en PDF ou sur Europresse le MediaCentre de l'ENT (puis onglet "Publications PDF" pour trouver le journal).*

## Manuels

📘 Manuel DELAGRAVE, pages 38-39  
👉 *Disponible sur elyco en PDF*

📘 Manuel NATHAN, pages 18-19  
👉 *Disponible sur elyco en PDF*

## Vidéos

▶️ **Du télégraphe à Internet : l’incroyable histoire des câbles sous-marins** - Les Echos (17 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="j07V-P7-MBo" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/j07V-P7-MBo
</p>

▶️ **Câbles sous-marins : la guerre invisible - Le Dessous des cartes** - Le Dessous des cartes - ARTE (12 min)

<div class="video-responsive">
    <div class="peertube_iframe centre" title="Cables sous marins la-guerre invisible" width="560" height="315" src="https://peertube.lyceeconnecte.fr/videos/embed/7083c463-19eb-4ed8-8f20-0ef719608253?warningTitle=0&amp;p2p=0" frameborder="0" allowfullscreen="" sandbox="allow-same-origin allow-scripts allow-popups"></div>
</div>
<p class="impression">
    Source vidéo : https://dgxy.link/wcbBr ou https://peertube.lyceeconnecte.fr/w/eTQJt8D4b3AaA6sCaKjKeV
</p>

▶️ **La trouvaille scandaleuse d’un hacker sur les câbles Internet sous-marins** - _Underscore (11 min)
<div class="video-responsive">
    <div class="youtube_player centre" videoID="vTC821fpwdw" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/vTC821fpwdw
</p>


# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

▶️ **Internet : pose d'un câble sous marin** - Guyane la 1ère (4 min) : <a href="https://youtu.be/fLpSdS9GSCc" target="_blank">voir la vidéo</a>

▶️ **Internet, nouvelles guerres** - Géopolitis (25 min) : <a href="https://youtu.be/Z-3O4SCbH80" target="_blank">voir la vidéo</a>

▶️ **Les câbles sous-marins, cibles de toutes les convoitises** - FRANCE 24 (4 min) : <a href="https://youtu.be/z8din7hS8Ok" target="_blank">voir la vidéo</a>

▶️ **Câbles sous-marins : l'autre guerre ?** - Le Dessous des cartes - L’essentiel - ARTE (2 min) : <a href="https://youtu.be/EGWYbyvTF2Q" target="_blank">voir la vidéo</a>

▶️ **Ukraine - L’internet russe : coupé du monde ?** - Le Dessous des cartes - L’essentiel - ARTE (2 min) : <a href="https://youtu.be/KspKRIc9ing" target="_blank">voir la vidéo</a>

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

