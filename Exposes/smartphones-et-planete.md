Les smartphones et la planète
=============================

<img class="centre image-responsive" src="data/smartphone_recyclage.jpg" alt="illustration : recyclage d'un smartphone" width="400">
<p class="legende">
    Crédits : Image par <a href="https://pixabay.com/fr/users/wir_sind_klein-6630807/?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2978601">Wilfried Pohnke</a> de <a href="https://pixabay.com/fr//?utm_source=link-attribution&utm_medium=referral&utm_campaign=image&utm_content=2978601">Pixabay</a>
</p>

<p class="description" markdown="1">

Les smartphones sont devenus des compagnons indispensables, nous reliant instantanément aux quatre coins de la planète. Mais cette révolution technologie a un impact environnemental très important. Alors, comment se déroule le cycle de vie complexe d'un smartphone ? Quelles en sont les implications, souvent méconnues, sur notre environnement et nos sociétés ? Et surtout, comment pouvons-nous, en tant qu'utilisateurs et citoyens, contribuer à réduire cet impact et forger un avenir où l'innovation coexiste harmonieusement avec la préservation de notre planète ?  
*Ce texte a été rédigé à l'aide de ChatGPT.*

</p>

# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :

- Le smartphone en chiffres
    - Évolution du nombre de smartphones fabriqués
    - Autres chiffres intéressants au regard de l'exposé
- Le cycle de vie d'un smartphone
    - Constituants
    - Fabrication
    - Distribution
    - Mort
- Conséquences ...
    - ... sur l'environnement
    - ... sur les populations
- Conseils pour limiter l'impact sur l'environnement (et les populations !)
    - Donner au moins 5 conseils concrets et facilement réalisables
    - Recyclage

</div>


# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne 

🌍 Guide pratique de l'ADEME et France Nature Environnement (FNE) : <a href="https://presse.ademe.fr/wp-content/uploads/2017/09/guide-pratique-impacts-smartphone.pdf" target="_blank">Les impacts du smartphone</a>

🌍 Panneau d'exposition de FNE : <a href="https://ged.fne.asso.fr/silverpeas/LinkFile/Key/e6e8caca-4a59-4a08-a618-9e1f9c383668/Panneau_expo_FNE_PdL_metaux_smartphone_oct2017.pdf" target="_blank">Votre smartphone est riche en métaux</a>

🌍 Infographie web *Qu'est-ce qu'on fait ?!* : <a href="https://archives.qqf.fr/infographie/52/smartphone" target="_blank">Le smartphone, une relation compliquée</a>

🌍 Article web de *01net* : <a href="https://www.01net.com/actualites/les-smartphones-reconditionnes-sont-ils-vraiment-plus-vertueux-2054022.html" target="_blank">Les smartphones reconditionnés sont-ils vraiment plus vertueux ?</a>

## Podcast

🎙️ **Un téléphone fait 4 fois le tour du monde avant d'être dans nos mains** (4 min) : un épisode (24 mars 2023) issu du podcast *Qu'est-ce qu'on fait ?!*, à écouter ci-dessous ou directement sur le site de <a href="https://www.radiofrance.fr/mouv/podcasts/qu-est-ce-qu-on-fait/un-telephone-fait-4-fois-le-tour-du-monde-avant-d-etre-dans-nos-mains-2149720" target="_blank">Radio France</a>

<div class="podcast-container">
    <img class="image-responsive centre"src="https://www.radiofrance.fr/s3/cruiser-production/2022/09/5a9a9375-7ec0-444f-8c3b-d2b1f29c1335/1400x1400_rf_omm_0000036483_ite.jpg" alt="icone podcast" width="200">
    <audio
        class="centre"
        controls
        preload="metadata"
        src="https://media.radiofrance-podcast.net/podcast09/23681-24.03.2023-ITEMA_23327455-2023J49477S0083-21.mp3">
    </audio>
</div>

## Vidéos

▶️ **Vie et mort d'un smartphone** - Mooc Impacts environnementaux du numérique (7 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="AJ8bA1iOnL8" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/AJ8bA1iOnL8
</p>

▶️ **5 faits sur l’impact environnemental du téléphone portable** - ciao.ch (5 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="Gf04WRYdQM0" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/Gf04WRYdQM0
</p>
    
▶️ **J'ai découvert comment sont recyclés nos vieux téléphones portables** #PlanB - Le Monde (6 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="s5LIdL5i9UI" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/s5LIdL5i9UI
</p>

▶️ **No Congo No Phone** - rappeur BM (paroles dans la description) (4 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="8JWD57H8u0Q" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/8JWD57H8u0Q
</p>




# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

📱 Formation courte [Le smartphone et la planète](https://epoc.inria.fr/epocs/E007MM/) accessible sur mobile via l'application [ePoc](https://epoc.inria.fr/) de l'INRIA téléchargeable sur Android et iOS (gratuite et open source)

🌍 Article de 01net.com : [Gabriel, 16 ans, créateur du téléphone à 30 euros](https://www.01net.com/actualites/gabriel-16-ans-createur-du-telephone-a-30-euros-tout-le-monde-a-un-smartphone-mais-personne-ne-sait-comment-il-fonctionne.html)

🎙️ **Smart Force**, série de très courts podcasts de l'ADEME, à écouter sur la plateforme *Ausha* : https://podcast.ausha.co/smart-force

<!-- <iframe name="Ausha Podcast Player" frameborder="0" loading="lazy" id="ausha-p8Yn" height="420" style="border: none; width:100%; height:421px" src="https://player.ausha.co/index.html?podcastId=nrnOqfq39NaO&playlist=true&v=3&playerId=ausha-p8Yn"> -->

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


