La neutralité du Net
====================

<img class="centre image-responsive" src="https://upload.wikimedia.org/wikipedia/commons/d/db/Symbole_de_la_neutralit%C3%A9_du_r%C3%A9seau_en_fran%C3%A7ais.svg" alt="illustration" width="250"/>

<p class="legende">Crédit : <a href="https://commons.wikimedia.org/wiki/File:Symbole_de_la_neutralit%C3%A9_du_r%C3%A9seau_en_fran%C3%A7ais.svg">Camillo Sanchez</a>, Domaine public, via Wikimedia Commons.</p>

<p class="description-seance" markdown="1">

Internet a modifié la façon dont les humains font société et l'un des principes fondateurs du réseau Internet s'appelle la *neutralité du net*. Mais qu'est-ce que la neutralité du net et comment a-t-elle évoluée au cours du temps ? Pourquoi est-elle parfois remise en cause ? Quels sont les dangers de la fin de la neutralité du net ?

</p>


# Consignes

Vous travaillerez en trinôme. Un diaporama est à préparer et une présentation orale d'environ 10 minutes est attendue.

<div class="contenu-expose" markdown="1">

### Contenu

Le diaporama et la présentation doivent permettre de présenter et analyser les éléments suivants :


- Qu'est-ce que la neutralité du net ?
    - Définition
    - Les grands principes
- La neutralité du net dans l'histoire et dans le monde :
    - Un principe fondamental d'Internet
    - Positions des différents pays sur la question
    - Évolution de la loi dans le monde
- Exemples concrets de situations dans le cas d'un arrêt de la neutralité du net
    - Les "anti" et les "pros" neutralité du net
    - Qui sont-ils ?
    - Les arguments des "pros" et des "anti" neutralité du net

</div>

# Ressources principales

Pour vous aider, voici des ressources sur lesquelles vous appuyer pour préparer l'exposé. Libre à vous d'en trouver davantage si vous le souhaitez.

## Ressources en ligne

🌍 Article de l'ARCEP : <a href="https://www.arcep.fr/nos-sujets/la-neutralite-du-net.html" target="_blank">La neutralité du net</a>

🌍 Guide l'ARCEP (PDF, 4 pages): <a href="https://www.arcep.fr/fileadmin/cru-1658411991/user_upload/arcep/nos-sujets/cartographie-net-neutralite-2018_conf050618.pdf" target="_blank">Tout comprendre des débats autour de la neutralité du net</a>

🌍 Article Wikipédia : <a href="https://fr.wikipedia.org/wiki/Neutralit%C3%A9_du_r%C3%A9seau" target="_blank">Neutralité du réseau</a>

🌍 Article du Monde, Pixels : <a href="https://www.lemonde.fr/pixels/article/2017/12/14/les-etats-unis-abrogent-la-neutralite-du-net-un-principe-fondateur-d-internet_5229906_4408996.html" target="_blank">Les Etats-Unis abrogent la neutralité du Net, un principe fondateur d’Internet</a>

🌍 Libération, tribune de Benjamin Bayart : <a href="https://www.liberation.fr/debats/2017/12/14/l-europe-doit-defendre-la-neutralite-du-net_1616667/" target="_blank">L'Europe doit défendre la « neutralité du Net »</a>

## Podcast

🎙️ **La neutralité du Net**, par Gilles BLB (podcast original : https://youtu.be/SGOC0zTlYBk)
<div class="podcast-container">
    <img class="image-responsive centre"src="data/podcast-icon.svg" alt="icone podcast" width="70">
    <audio
        class="centre"
        controls
        preload="metadata"
        src="data/podcast_la_neutralite_du_net.mp3">
            <a href="data/podcast_la_neutralite_du_net.mp3">
                Télécharger le podcast
            </a>
    </audio>
</div>
<p class="legende">Crédits : Textes : Gilles BLB (<a href="http://gillesblb.fr/">http://gillesblb.fr/</a>), licence <a href="https://creativecommons.org/licenses/by-nc-sa/4.0/deed.fr">CC BY-NC-SA</a>
<br>Musique by Lone Runner (<a href="https://lonerunner.bandcamp.com/">https://lonerunner.bandcamp.com/</a>) - Tous droits réservés</p>

## Vidéos

▶️ **Qu’est-ce que la neutralité du Net ?** - Le Monde (3 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="A3dElmSY8q4" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/A3dElmSY8q4
</p>

▶️ **Pourquoi faut-il défendre la neutralité du net ?** - Le Fil d'Actu (6 min) - INRIA Flowers (5 min) 

<div class="video-responsive">
    <div class="youtube_player centre" videoID="jQs3Jx_LP54" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/jQs3Jx_LP54
</p>

▶️ **Neutralité, j'écris ton nom** - Data Gueule (3 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="hZnq3xg-PRM" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/hZnq3xg-PRM
</p>

▶️ **Neutralité du net : Internet va-t-il coûter plus cher ?** - On n'est plus des pigeons ! (11 min)

<div class="video-responsive">
    <div class="youtube_player centre" videoID="mcN156dQObw" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source vidéo : https://youtu.be/mcN156dQObw
</p>


# Autres ressources

Voici quelques ressources supplémentaires pour celles et ceux qui veulent en savoir davantage.

🌍 Conférence de Benjamin Bayart sur la neutralité du net (41 min) : <a href="https://youtu.be/i7JVzN3dM34" target="_blank">voir la vidéo</a>

🎙️ Faut-il sauver la neutralité du net ? : <a href="https://www.radiofrance.fr/franceculture/podcasts/du-grain-a-moudre/faut-il-sauver-la-neutralite-du-net-5963376" target="_blank">écouter le podcast</a>

🌍 Article du Monde - Pixels : <a href="https://www.lemonde.fr/pixels/article/2017/12/11/steve-wozniak-tim-berners-lee-l-appel-des-pionniers-pour-defendre-la-neutralite-du-net_5228170_4408996.html" target="_blank">Steve Wozniak, Tim Berners-Lee… L’appel des pionniers pour défendre la neutralité du Net</a>

🌍 Article du Monde - Pixels : <a href="https://www.lemonde.fr/pixels/article/2017/12/15/la-neutralite-du-net-expliquee-avec-des-camions_5230070_4408996.html" target="_blank">La neutralité du Net expliquée avec des camions</a>


<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)


