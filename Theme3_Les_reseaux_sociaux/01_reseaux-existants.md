# Réseaux sociaux existants

Dans cette séance, on donne quelques repères historiques sur l'histoire des réseaux sociaux puis on dresse un portrait des médias sociaux actuels. On en profite pour évoquer leur modèle économique : comment ces entreprises gagnent-elles autant d'argent alors même que vous pouvons utiliser leurs plateformes gratuitement ?

<img class="centre image-responsive" src="data/panorama_rs_2022.webp" alt="illustration" width="800"/>
<p class="legende">
    <strong>Panorama des médias sociaux en 2022.</strong>
    <br>Source : Fred Cavazza, <a href="https://fredcavazza.net/2022/06/14/panorama-des-medias-sociaux-2022/" target="_blank">Panorama des médias sociaux 2022</a>.</p>

# Diaporama d'introduction

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/reseaux_sociaux/data/Reseaux_sociaux_tour_dhorizon.pdf"
        class = "centre"
        width="800"
        height="600"
        style="border: none;">
    </iframe>
</div>
<p class="impression">
    Source diaporama : <a href="https://info-mounier.fr/snt/reseaux_sociaux/data/Reseaux_sociaux_tour_dhorizon.pdf">https://info-mounier.fr/snt/reseaux_sociaux/data/Reseaux_sociaux_tour_dhorizon.pdf</a>
</p>

# Données personnelles et réseaux sociaux

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Regardez la vidéo ci-dessous et répondez aux questions qui suivent.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="MXwKr5wGFwU" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/MXwKr5wGFwU">https://youtu.be/MXwKr5wGFwU</a>
</p>

</div>



✍️ **Question 1** : Listez les différentes actions sur les réseaux sociaux qui participent à créer notre *identité numérique*.

✍️ **Question 2** : Quelle est la principale source de revenu de la plupart des médias sociaux ? Pourquoi ont-ils besoin de récolter des données personnelles ?

✍️ **Question 3** : Que peut-on faire pour maîtriser davantage notre identité numérique ?

# L'économie de l'attention

L'objectif des médias sociaux est de garder notre attention le plus longtemps possible. Ils ont pour cela mis en oeuvre des techniques pour constamment nous garder connectés, à tel point que leur modèle économique s'appelle désormais l'**économie de l'attention**. 

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Regardez les trois vidéos, lisez l'extrait de l'article proposé et répondez aux questions qui suivent.

</div>

**Vidéo 1 : C'est quoi l'économie de l'attention ?**

<div class="video-responsive">
    <div class="youtube_player centre" videoID="qtAvaE2YxV0" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/qtAvaE2YxV0">https://youtu.be/qtAvaE2YxV0</a>
</p>

**Document : Interview de Tristan Harris** (source : <a href="https://dgxy.link/LVqTP" target="_blank">https://dgxy.link/LVqTP</a>)

<blockquote class="information">
    <p>Pour information, Tristan Harris travaillait chez Google jusqu'en 2016 et milite désormais contre les géants du numérique qui nous volent notre attention.</p>
</blockquote>

<div class="a-lire">
    <p><strong><em>Tristan Harris : votre attention, s'il vous plaît</em></strong></p>
    <p>« <em>YouTube sait de mieux en mieux prévoir quelle vidéo il doit lancer pour te garder devant l’écran, même si cela te prive de sommeil. Instagram excelle à te montrer quelque chose que tu serais en train de rater, ou quelqu’un dont tu devrais être jaloux.</em> » Comme les machines à sous, les applications parviennent à entretenir la flamme, à coups d’abonnés gagnés sur Twitter ou de streaks (classement des relations) sur Snapchat. Parfaites pour l’ego, ces petites sucreries ne compensent pas de nombreux effets négatifs. « <em>Plus tu es connecté, plus tu ressens d’anxiété, affirme Harris. Certaines applications indiquent par exemple qu’un message a été lu par son destinataire. Cela crée une sorte d’obligation à répondre rapidement, sinon tu passes pour un mauvais ami.</em> »
    <p style="text-align:right">Source : Extrait de l'article <em>Tristan Harris : votre attention, s'il vous plaît</em>, Libération, 22 décembre 2017. <br>Voir l'<a href="https://www.liberation.fr/futurs/2017/12/22/tristan-harris-votre-attention-s-il-vous-plait_1618567/" target="_blank">article complet</a>.</p>
</div>

**Vidéo 2 : Extrait d'un reportage d'Envoyé Spécial**

<div class="video-responsive">
    <iframe class="centre" width="560" height="315" src="//embed.francetv.fr/8e627144cb474bfc38b71a84366531f9" frameborder="0" scrolling="no" allowfullscreen></iframe>
</div>
<p class="impression">
    ▶️ Source : <a href="https://dgxy.link/u3o3K">https://dgxy.link/u3o3K</a>
</p>

**Vidéo 3 : Extrait d'un reportage d'Enquête de santé**

<div class="video-responsive">
    <div class="dailymotion_player centre" videoID="x7ubej3" width="560" height="315" showinfo="1" autoplay="0" embedType="video"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://www.dailymotion.com/video/x7ubej3">https://www.dailymotion.com/video/x7ubej3</a>
</p>

✍️ **Question 4** : Quel est l'intérêt des réseaux sociaux à maintenir leurs utilisateurs devant leur écran ?

✍️ **Question 5** : Quelles sont les différentes techniques évoquées dans les vidéos et dans l'article pour capter notre attention ?

✍️ **Question 6** : Comment ces techniques exploitent-elles le fonctionnement de notre cerveau ?

✍️ **Question 7** : Quelles solutions peut-on mettre en place pour réussir à mieux garder le contrôle ?

<blockquote class="information">
    <p>Le reportage complet dont est extrait la dernière vidéo est disponible à cette adresse : <a href="https://youtu.be/st1iRyaAXvU" target="_blank">https://youtu.be/st1iRyaAXvU</a>.</p>
</blockquote>

---
**Références** :
- Les références pour le diaporama sont indiquées en dernière page de celui-ci.

---
Les enseignants du lycée Emmanuel Mounier à Angers 

![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)