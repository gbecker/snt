# Modélisation d'un réseau social par un graphe

Dans cette séance, on étudie comment un réseau social peut être modélisé mathématiquement. Cette modélisation permet de représenter les relations entre les personnes et il est alors possible d'appliquer toute sorte d'algorithmes : de recommandation, d'analyse de comportement, de calculs, etc.

<img class="centre image-responsive" src="data/graphe.svg" alt="illustration d'un réseau social par un graphe" width="250">
<p class="image-licence">Crédit : <a href="https://pixabay.com/fr/vectors/connexions-communication-social-2099068/" target="_blank">GDJ</a> via Pixabay.</span></p>

# Représenter graphiquement un réseau social

## Notre réseau social

Soit un ensemble d’amis connectés sur un réseau social quelconque. Voici les interactions que l’on a recensées :

*   André est ami avec Béa, Charles, Estelle et Fabrice ;
*   Béa est amie avec André, Charles, Denise et Héloïse ;
*   Charles est ami avec André, Béa, Denise, Estelle, Fabrice et Gilbert ;
*   Denise est amie avec Béa, Charles et Estelle ;
*   Estelle est amie avec André, Charles et Denise ;
*   Fabrice est ami avec André, Charles et Gilbert ;
*   Gilbert est ami avec Charles et Fabrice ;
*   Héloïse est amie avec Béa.

<blockquote class="information" markdown="1">
Pas facile de se représenter les liens qui existent entre eux, et encore il n'y a que 8 personnes, imaginez un réseau social de plusieurs millions d’abonnés 🤔 ! Essayons de représenter cela par un schéma, vous verrez c'est beaucoup plus parlant !
</blockquote>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 1

On va représenter le réseau social de la façon suivante :

*   Chaque personne est symbolisée par son initiale (A pour André, B pour Béa, etc.) écrite dans un cercle ;
*   Un lien d’amitié est symbolisé par un trait entre deux amis.

1.  Dessinez la représentation du réseau social avec les consignes ci-dessus.
2.  Écrivez à côté de chaque initiale le nombre d’amis de chaque personne (il suffit de compter les liens qui partent de chaque initiale).
3.  Quelle est la personne qui a le plus d’amis ? Et celle qui en a le moins ?
4.  Quelle distance, en nombre de liens, sépare Béa d’Estelle ?
5.  Existe-t-il dans ce schéma deux personnes éloignées de plus de 2 liens ? Justifier.

</div>


## Introduction du vocabulaire

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 2

Regardez la vidéo ci-dessous, **du début à 3 min 37 s**, puis répondez aux questions suivantes.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="YYv2R1cCTa0" end="217" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/YYv2R1cCTa0?end=217">https://youtu.be/YYv2R1cCTa0?end=217</a>
</p>

1.  Quel nom porte le schéma dessiné dans le “À faire n°1” ?
2.  Dans ce schéma :
    *   que représentent les **sommets** ?
    *   que représentent les **arêtes** ?
3.  Mis à part les réseaux sociaux, donnez d’autres exemples de relations pouvant être représentées par un graphe.

</div>

# Caractéristiques d’un graphe

## Notions de distance et d’écartement

Voici deux définitions importantes :

<div class="important">
    <ul>
        <li>La <strong>distance entre deux sommets</strong> est le nombre minimum d’arêtes qu’il faut parcourir pour aller d’un des deux sommets à l’autre.</li>
        <li>L'<strong>écartement d'un sommet</strong> (ou <em>excentricité</em>) est la distance maximale entre ce sommet et les autres sommets du graphe.</li>
    </ul>
</div>

**Exemple** : Le graphe de notre réseau social est le suivant :

<img class="centre image-responsive" src="data/vrai_graphe1.png" alt="un graphe">

Pour aller du sommet A au sommet H, on peut prendre le chemin : A - E - C - B - H, soit un chemin passant par trois arêtes. Mais il existe un chemin passant uniquement par deux arêtes : A - B - H. On ne peut pas faire plus court donc **la distance entre les sommets A et H est égale à 2**, on note cela $\text{distance}(A, H) = 2$.

Si on observe la distance entre le sommet A et chacun des autres sommets du graphe, on obtient les distances suivantes : $\text{distance}(A,A) = 0$ , $\text{distance}(A,B) = 1$ , $\text{distance}(A,C) = 1$ , $\text{distance}(A,D) = 2$ , $\text{distance}(A,E) = 1$ , $\text{distance}(A,F) = 1$ , $\text{distance}(A,G) = 2$ et $\text{distance}(A,H) = 2$.

On constate que la distance maximale entre le sommet A et les autres sommets du graphe est égale à 2, donc l’**écartement du sommet A est égal à 2**.

<blockquote class="information">
    <p>On peut avantageusement s’aider d’un tableau pour reporter toutes les distances entre les sommets et en déduire l’écartement de chaque sommet. C’est ce qui est proposé ci-dessous.</p>
</blockquote>


<div class="a-faire titre" markdown="1">

### ✏️ Exercice 3

1.  Recopiez et complétez les lignes manquantes du tableau suivant qui donne la distance entre les sommets du graphe de notre réseau social.
2.  Complétez ensuite la dernière colonne avec l’écartement de chaque sommet.

<div class="table-container">
<table class="tg derniere-colonne-importante">
<thead>
<tr>
<th style="width:30px"></th>
<th style="width:30px">A</th>
<th style="width:30px">B</th>
<th style="width:30px">C</th>
<th style="width:30px">D</th>
<th style="width:30px">E</th>
<th style="width:30px">F</th>
<th style="width:30px">G</th>
<th style="width:30px">H</th>
<th>Écartement</th>
</tr>
</thead>
<tbody>
<tr>
<td>A</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>2</td>
<td>2</td>
<td>2</td>
</tr>
<tr>
<td>B</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>C</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>D</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>2</td>
<td>2</td>
<td>2</td>
<td>2</td>
</tr>
<tr>
<td>E</td>
<td>1</td>
<td>2</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>2</td>
<td>2</td>
<td>3</td>
<td>3</td>
</tr>
<tr>
<td>F</td>
<td>1</td>
<td>2</td>
<td>1</td>
<td>2</td>
<td>2</td>
<td>0</td>
<td>1</td>
<td>3</td>
<td></td>
</tr>
<tr>
<td>G</td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>H</td>
<td>2</td>
<td>1</td>
<td>2</td>
<td>2</td>
<td>3</td>
<td>3</td>
<td>3</td>
<td>0</td>
<td></td>
</tr>
</tbody>
</table>
</div>

</div>

## Diamètre, centre et rayon d’un graphe

Voici les définitions de ces trois notions :

<div class="important">
    <ul>
        <li>On appelle <strong>diamètre</strong> d'un graphe, la distance maximale entre deux sommets de ce graphe (c'est donc l'écartement maximal des sommets).</li>
        <li>On appelle <strong>centre</strong> d'un graphe, l'ensemble des sommets dont l'écartement est minimal.</li>
        <li>On appelle <strong>rayon</strong> d'un graphe, l'écartement des sommets du centre du graphe (c'est donc l'écartement minimal des sommets).</li>
    </ul>
</div>


<div class="a-faire titre" markdown="1">

### ✏️ Exercice 4

On considère ici le graphe du réseau social de départ (“À faire n°1”).  
En lisant attentivement les trois définitions données au-dessus, répondez aux questions suivantes. _Aidez-vous du tableau construit précédemment dans le “À faire n°3”_.

1.  Quel est le diamètre du graphe représentant notre réseau social ? Justifiez.
2.  Quel est le centre de ce graphe (il peut y avoir plusieurs sommets) ? Justifiez.
3.  Quel est le rayon de ce graphe ? Justifiez.

</div>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 5

Voici un graphe représentant un autre réseau social.

<img class="centre image-responsive" src="data/graphe2.png" alt="un autre graphe">

1.  Déterminez le diamètre, le centre et le rayon de ce graphe. _Vous construirez un tableau comme précédemment pour vous aider._
2.  Dans cette question vous êtes un publicitaire.
    *   Quelle personne contacteriez-vous pour qu’elle présente votre produit sur ce réseau social ? Pourquoi ?
    *   Quel nom porte ce genre de personnes ?

</div>

# Autre façon de représenter un graphe

On reprend le graphe de notre réseau social de départ. On peut aussi utiliser un tableau pour représenter ce graphe. Dans ce tableau, il suffit de mettre un “1” pour signifier une relation (d’amitié) et un “0” pour signifier une absence de relation entre deux personnes.

Cela donnerait le tableau suivant :

<table class="tg">
<thead>
<tr>
<th style="width:25px"></th>
<th style="width:25px">A</th>
<th style="width:25px">B</th>
<th style="width:25px">C</th>
<th style="width:25px">D</th>
<th style="width:25px">E</th>
<th style="width:25px">F</th>
<th style="width:25px">G</th>
<th style="width:25px">H</th>
</tr>
</thead>
<tbody>
<tr>
<td>A</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>B</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>1</td>
</tr>
<tr>
<td>C</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>D</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>E</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>F</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>0</td>
</tr>
<tr>
<td>G</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>H</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
<td>0</td>
</tr>
</tbody>
</table>

<blockquote class="information">
    <p>Ce tableau est appelé <em>tableau d’adjacence</em> ou <em>matrice d’adjacence</em> du graphe. Il peut facilement être représenté dans la mémoire d’un ordinateur puisqu’il <em>traduit</em> le graphe par une série de “0” et de “1”.</p>
</blockquote>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 6

On donne ci-dessous le tableau d’adjacence d’un graphe représentant un réseau social.

<div class="table-container">
<table class="tg">
<thead>
<tr>
<th></th>
<th>Mick</th>
<th>Léa</th>
<th>Jade</th>
<th>Paul</th>
<th>Marc</th>
</tr>
</thead>
<tbody>
<tr>
<td>Mick</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>Léa</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>1</td>
</tr>
<tr>
<td>Jade</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>1</td>
<td>1</td>
</tr>
<tr>
<td>Paul</td>
<td>0</td>
<td>0</td>
<td>1</td>
<td>0</td>
<td>0</td>
</tr>
<tr>
<td>Marc</td>
<td>0</td>
<td>1</td>
<td>1</td>
<td>0</td>
<td>0</td>
</tr>
</tbody>
</table>
</div>

1.  Quels sont les amis de Léa ?
2.  Dessinez le graphe correspondant à ce tableau d’adjacence.
3.  Déterminez à la main le diamètre, le centre et le rayon de ce graphe. Vous pourrez vérifier avec Python vos résultats dans la fenêtre de code précédente.

</div>

---

**Références** :

- La première partie (le “À faire n°1”), quoique raccourcie, est inspirée d’une [activité](https://drive.google.com/file/d/11MCdeG6FaodPP3AglhsKnR4jcVEmiXYv/view) proposée par Gilles Lassus, elle-même basée sur une [vidéo](https://youtu.be/MySkCFFgiRQ) de Mickaël Launay.

---
Les enseignants du lycée Emmanuel Mounier à Angers ![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)