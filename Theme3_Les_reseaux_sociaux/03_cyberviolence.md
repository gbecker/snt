La cyberviolence
================

<div class="effacer-impression" markdown="1">

**Quel est le point commun entre ces personnes ? ...**

<img class="centre image-responsive" src="data/suicides.gif" alt="eleves suicides" width="300">

**... Elles se sont toutes suicidées, après avoir été harcelées à l'école et sur les réseaux sociaux.**

</div>

<p><em>D'après une étude de l'UNESCO de 2020, 7 jeunes sur 10 sont victimes de cyberharcèlement avant d'avoir atteint l'âge de 18 ans. Le phénomène s'est très largement accentué avec l'utilisation des réseaux sociaux. Il est important d'être informé sur ce que sont les différentes formes de cyberviolence, comment y faire face lorsqu'on les subit ou lorsqu'on en est témoin, mais aussi de connaître les sanctions prévues par la justice française contre les harceleurs.</em></p>

<img class="centre image-responsive" src="data/hashtag_non_au_harcelement.png" alt="hashtag numero non au harcelement" width="400">

# Un témoignage en podcast

L'épisode suivant, issu du podcast [Suis ta Voix !](https://podcast.ausha.co/suis-ta-voix), a été écrit et enregistré par **Absa Diallo** et **Assma Wague**, deux lycéennes et ambassadrices de l'association [Becomtech](https://becomtech.fr/), investies à promouvoir la mixité dans les filières du numérique.

<img class="centre image-responsive" alt="Absa Diallo et Assma Wague" src="data/Absa_Diallo_%26_Assma_Wague.jpg" width="300">
<p class="legende">
    <strong>Absa Diallo et Assma Wague</strong>
    <br>Crédit : <a href="https://twitter.com/suistavoix/status/1633068375713751041?s=20">@suistavoix</a>, Twitter
</p>

Il s'agit d'un témoignage fort qui retrace le chemin des victimes, tout en apportant soutien et bienveillance à celles et ceux qui vivent cette terrible expérience.

<div class="a-faire titre" markdown="1">

### ✏️ Travail demandé

Écoutez le podcast ci-dessous et répondez aux questions qui suivent (qu'il peut être utile de lire avant l'écoute !).

<div class="ausha_player effacer-impression" data-height="220" data-podcast-id="b2MO7iLQVda0" data-player-id="ausha-DYTp" data-playlist="" data-color="" data-useshowid="0"></div>

<p class="impression">
    🎙️ Source : <a href="https://smartlink.ausha.co/suis-ta-voix/bien-plus-que-des-textos-le-cyberharcelement" target="_blank">https://smartlink.ausha.co/suis-ta-voix/bien-plus-que-des-textos-le-cyberharcelement</a>
</p>

</div>

✍️ **Question 1** : Pourquoi les outils numériques, et principalement les réseaux sociaux, amplifient les phénomènes de cyberharcèlement ?

✍️ **Question 2** : Définir le terme *cyberharcèlement* en une phrase simple.

✍️ **Question 3** : Listez différentes formes de cyberharcèlement.

✍️ **Question 4** : Quelles sont les conséquences psychologiques sur la santé ?

✍️ **Question 5** : Comment expliquer le comportement des harceleurs ?

✍️ **Question 6** : Que faire lorsqu'on est victime ou témoin de cyberharcèlement ?

✍️ **Question 7** : Quelles sont les responsabilités des plateformes numériques ?

✍️ **Question 8** : Quelles sont solutions proposées au lycée Mounier pour prendre en compte la parole des victimes et témoins ?


<details class="deroulement-simple effacer-impression" markdown="1">

<summary>Éléments de réponse</summary>

1. L'anonymat (impunité => libération de l'agressivité en ligne), diffusion simple, rapide et massive
2. Actes agressifs et répétés de manière intentionnelle aux moyens de médias numériques à l'encontre d'une personne
3. Usurpation d'identité, création de faux compte, insultes, menaces, revenge porn (porno divulgation)
4. Anxiété, confiance en soi, trouble du sommeil, baisse d'intérêt pour ses passions, baisse résultats scolaires, repli sur soi, se faire du mal
5. Il sont souvent mal dans leur peau, effet de groupe, pas de réflexion sur les conséquences car mal accompagnés (par les adultes) dans une utilisation éthique des outils numériques
6. Ne pas avoir honte, en parler à des personnes de confiance, ou l'écrire, pour ne plus être seul ! Numéro 3018 pour se confier et avoir des conseils (téléphone, application smartphone). Porter plainte contre les harceleurs, contre l'hébergeur du site. Autre exemple : StopFisha (asso contre le cybersexisme)
7. Mettre en place des mécanismes de modération, ne pas publier des contenus haineux, de cyberviolence, ou les supprimer rapidement.
8. Les z'attentifs = élèves formés pour écouter et faire preuve de médiation au sein de l'établissement. Les adultes : infirmière, CPE, AED, enseignants, etc.

</details>

# Les différentes formes de cyberviolence

<p class="important">
    La <strong>cyberviolence</strong> se définit comme un acte agressif, intentionnel, perpétré par un individu ou un groupe aux moyens de médias numériques à l'encontre d'une ou plusieurs victimes. Lorsque des faits de cyberviolence sont répétés de manière délibérée, on parle de <strong>cyberharcèlement</strong>.
</p>

Les formes de cyberharcèlement les plus courantes sont :

- Harcèlement / Exclusion
- Dénigrement
- Usurpation d'identité
- Happy Slapping
- Outing
- Sexting

✍️ **Question 9** : Asscociez chacune de ces formes à sa signification :

1. Décrédibilisation d’une personne en portant **atteinte à son image ou à sa e-réputation**, en lançant toutes sortes de rumeurs à son égard. L’auteur des violences publie par exemple une photographie humiliante (parfois truquée), sur son mur ou directement sur celui de la victime, et incite ses contacts à écrire des commentaires désobligeants.
2. Captation, le plus souvent à l’aide d’un téléphone portable, d’une **scène de violence** subie par une personne et diffusion en ligne.
3. Publication de **commentaires insultants** ou de rumeurs sur le « mur » ou le profil de la victime, dans le but de l’**isoler** du groupe de pairs/de membres du réseau. Une des formes particulièrement « en vogue » est le **flamming** : une salve de messages insultants/menaçants à destination d’une personne ou d’un groupe de personnes.
4. Contraction des mots sex (sexe) et texting (envoi de SMS). Il s’agit de textos, de photographies ou de vidéo à caractère **explicitement sexuel** dans le but de séduire son/sa partenaire. Mais lorsque ces photographies ou vidéos sont interceptées, puis **diffusées en ligne par un tiers malveillant** cherchant à nuire à la personne qu’elles représentent, il s’agit d’une cyberviolence. Lorsque les photographies ou vidéos intimes sont publiées **à des fins de vengeance** par un(e) ex-petit(e) ami(e) qui vit mal la rupture et souhaite nuire à l’autre, on parle de « revenge porn ».
5. **Accès à la messagerie ou au profil de la victime**, en se faisant passer pour elle afin d’envoyer des messages embarrassants/insultants à une autre personne. Elle peut aussi prendre la forme d’un **faux profil** ouvert au nom de la personne ciblée.
6. **Divulgation d’informations intimes et/ou confidentielles** sur une personne. Par exemple, révélation, sans qu’elle le sache ou ne le veuille, de son homosexualité.

# Que dit le Code Pénal ?

C'est l'article 222-33-2-2 du Code pénal, reproduit ci-dessous, qui définit les situations relevant du harcèlement ainsi que les sanctions prévues par la justice française contre les harceleurs.

<div class="a-lire">
    <p><strong>Article 222-33-2-2 du Code pénal - Loi du 3 août 2018</strong></p>
    <p>
        Le fait de harceler une personne par des propos ou comportements répétés ayant pour objet ou pour effet une dégradation de ses conditions de vie se traduisant par une altération de sa santé physique ou mentale est puni d'un an d'emprisonnement et de 15 000 € d'amende lorsque ces faits ont causé une incapacité totale de travail inférieure ou égale à huit jours ou n'ont entraîné aucune incapacité de travail.
    </p>
    <p>
        L'infraction est également constituée :
    </p>
    <p>
        a) Lorsque ces propos ou comportements sont imposés à une même victime par plusieurs personnes, de manière concertée ou à l'instigation de l'une d'elles, alors même que chacune de ces personnes n'a pas agi de façon répétée ;
    </p>
    <p>
        b) Lorsque ces propos ou comportements sont imposés à une même victime, successivement, par plusieurs personnes qui, même en l'absence de concertation, savent que ces propos ou comportements caractérisent une répétition.
    </p>
    <p>
        Les faits mentionnés aux premier à quatrième alinéas sont punis de deux ans d'emprisonnement et de 30 000 € d'amende :
    </p>
    <p>
        1° Lorsqu'ils ont causé une incapacité totale de travail supérieure à huit jours ;
    </p>
    <p>
        2° Lorsqu'ils ont été commis sur un mineur de quinze ans ;
    </p>
    <p>
        3° Lorsqu'ils ont été commis sur une personne dont la particulière vulnérabilité, due à son âge, à une maladie, à une infirmité, à une déficience physique ou psychique ou à un état de grossesse, est apparente ou connue de leur auteur ;
    </p>
    <p>
        4° Lorsqu'ils ont été commis par l'utilisation d'un service de communication au public en ligne ou par le biais d'un support numérique ou électronique ;
    </p>
    <p>
        5° Lorsqu'un mineur était présent et y a assisté.
    </p>
    <p>
        Les faits mentionnés aux premier à quatrième alinéas sont punis de trois ans d'emprisonnement et de 45 000 € d'amende lorsqu'ils sont commis dans deux des circonstances mentionnées aux 1° à 5°.
    </p>
    <p style="text-align:right"><em>Source : <a href="https://bit.ly/2RQWHh1">https://bit.ly/2RQWHh1</a></em></p>
</div>

✍️ **Question 10** : Deux lycéens de 16 ans, en présence l'un de l'autre, pour faire comme plusieurs de leurs amis, ont chacun envoyé *un* message d'insultes à un camarade de leur classe via une application de messagerie. Ce camarade s'est replié sur lui-même, au point de ne plus être capable de venir au lycée. Selon la loi, que risque chacun des deux lycéens du début ?

# Non au harcèlement

Voici la vidéo lauréate du prix "Non au harcèlement" 2023 (niveau lycée) :

<div class="video-responsive">
    <div class="youtube_player centre" videoID="IePYIuIXx0Q" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/IePYIuIXx0Q">https://youtu.be/IePYIuIXx0Q</a>
</p>
<p class="legende effacer-impression">
    <strong>Prix "Non au harcèlement" 2023, Meilleure vidéo lutte contre le harcèlement, niveau Lycée.</strong>
    <br>Source : <a href="https://www.education.gouv.fr/non-au-harcelement/laureats-du-concours-non-au-harcelement-323019">education.gouv.fr/non-au-harcelement/</a>.
</p>


<img class="centre image-responsive" src="data/non_au_harcelement_2023.png" alt="affiche non au harcelement">

✍️ **Question 11** : À quoi servent les numéros 3020 et 3018 ?

<br>

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)