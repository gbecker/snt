Les images numériques
=====================

Dans cette séance on s'intéresse aux images numériques. On définira la notion de pixels et la façon dont sont représentés les pixels d'une image couleur. On parlera également de la **définition** et de la **profondeur de couleur** d'une image numérique, mais aussi de la **résolution** et de la **taille** d'une image.

<img class="centre image-responsive" src="data/illustration_pixel.png">
<p class="legende">
    <strong>Des pixels.</strong>
    <br>Crédits : <a href="https://commons.wikimedia.org/wiki/File:Fil-Pixel-example-wiki.png">BlueEel</a> at Danish Wikipedia, <a href="https://creativecommons.org/licenses/by-sa/3.0">CC BY-SA 3.0</a>, via Wikimedia Commons.
</p>

# Définition d'une image

Une image numérique est un ensemble de points appelés **pixels**.

<div class="important" markdown="1">

La **définition** d'une image est le nombre de pixels constituant l'image. Cette définition est souvent donnée sous la forme $m \times n$ où $m$ et $n$ sont les nombres de pixels de l'image en largeur et en hauteur. Par exemple, une image 4000 x 3000 a une définition de 12 000 000 pixels soit 12 megapixels.

</div>

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Cliquez <a href="https://www.flickr.com/photos/teamdroid/5191470024/in/photolist-8UKDaS-2nh3x8d-aBwYFt-4H6URQ-21DtQNq-sd8vJ-8M51yD-FyrMLo-4K4w5L-4sVw5X-4sVuER-Kogh9-6bH3dV-29CzEbF-2mYh8oZ-nsdWz4-nqKNBm-4iYjjy-FjvRDz-bRGWZt-bFTcEn-c8Uv5b-zYSQLg-4vtDRw-tiEcY-Gcumuw-67Kyff-22SJGJV-6FJGsy-8UKD1Q-8UKCWJ-84e2sN-bsYdR7-4X6Zt4-aMKpi-Fjkmaf-bFT7Tv-bCjHh2-pAPS4y-zjmLrY-AgopEe-zjuDmi-4zRMhX-6cVh8J-zYNr2Q-AhmYZz-Ae66dw-zjmLAL-4zRM16-Agoq1z" target="_blank">ici</a> pour accéder à une image sur une banque d'images, puis répondez aux questions ci-dessous.

</div>

✍️ **Question 1** : Quelle est la licence de cette image ? Expliquez ce que cela signifie.

✍️ **Question 2** : Cliquez en bas à droite sur l'icône "Télécharger" puis sélectionnez "Affichez toutes les tailles" (le mot "taille" est par ailleurs mal choisi car il désigne autre chose, voir plus bas). Quelle est la définition la plus élevée pour cette image (celle prise par l'appareil photo numérique) ?

✍️ **Question 3** : Quelle est la définition de la version "miniature" ?

# Les pixels d'une image et leur codage

## Le codage RVB

La couleur d'un pixel est représenté par trois valeurs : celle du rouge (R), celle du vert (V) et celle du bleu (B).

En effet, en "mélangeant" une certaine intensité de chacune de ses trois couleurs, on peut obtenir toutes les couleurs, c'est le principe physique de la *synthèse additive des couleurs*.

<img class="centre image-responsive" src="data/Synthese_additive.png" alt="Schéma de la synthèse additive" width="250">
<p class="legende">
    <strong>Schéma de la synthèse additive des couleurs.</strong>
    <br>Crédits : <a href="https://fr.wikipedia.org/wiki/Synth%C3%A8se_additive#/media/Fichier:Synthese+.svg" target="_blank">Quark67</a>, Licence <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.fr" target="_blank">CC BY-SA 3.0</a>.
</p>

Pour mémoriser une image, il suffit de mémoriser chacun de ses pixels. Comme un pixel est définit par ses 3 composantes R, V et B, il suffit de mémoriser ces trois valeurs pour chaque pixel.

<div class="important">
    <p>La <strong>profondeur de couleur</strong> d'une image est le nombre de bits utilisés pour coder la couleur d'un pixel. La images couleur classiques ont une profondeur 24 bits (3 octets), c'est-à-dire que chaque composante R, V et B est codée sur 8 bits (1 octet), donc par un nombre entier compris entre 0 et 255. Ainsi, on peut donner la couleur d'un pixel par un triplet $(R, V, B)$, par exemple $(212, 80, 127)$ qui signifie que $R = 212$, $V = 80$ et $B = 127$.</p>
</div>

Plus la profondeur de couleur est élevée, plus la palette de couleurs possibles est grande ! Néanmoins, une profondeur de couleur de 24 bits est largement suffisante la plupart du temps.

✍️ **Question 4** : On dispose une image dont la définition est 1024 * 768 et dont la profondeur de couleur est 24 bits. Calculer la taille, en octet, du fichier correspondant à cette image.

## Pouvoir séparateur de l'oeil

Notre oeil ne peut pas le voir, mais lorsque l'on observe le pixel d'un écran (d'ordinateur, de smartphone, de TV, etc.) à la loupe, on peut constater qu'il est constitué de trois parties : une partie rouge, une partie verte et une partie bleue comme sur la photo ci-dessous.

<img class="centre image-responsive" src="data/pixels_smartphone.jpg" alt="Schéma de la synthèse additive" width="200">
<p class="legende">
    <strong>Écran d'un smartphone affichant la couleur blanche.</strong>
    <br>Crédits : <a href="https://commons.wikimedia.org/wiki/File:Cell_Phone_screen_Pixels.jpg" target="_blank">Dinesh Dhankhar</a>, Licence <a href="https://creativecommons.org/licenses/by/4.0/deed.fr" target="_blank">CC BY 4.0</a>.
</p>

Pourquoi notre oeil ne peut pas le voir ? Car il est limité par son pouvoir séparateur ! Deux points trop proches l'un de l'autre ne peuvent pas être distingués l'un de l'autre par notre oeil, qui les superposera. Par exemple, si vous vous éloignez de votre écran, les trois images ci-dessous sembleront identiques à partir d'un certaine distance.

<img class="centre image-responsive" src="data/differentes_resolutions.jpg" width="800">
<p class="legende">
    <strong>Trois images d'une voiture, d'une haute résolution à une basse résolution.</strong> 
    <br>Crédits : <a href="https://commons.wikimedia.org/wiki/File:Resolution_test.jpg">Thegreenj~commonswiki</a> supposé ((étant donné la revendication de droit d’auteur).</a>, <a href="http://creativecommons.org/licenses/by-sa/3.0/">CC BY-SA 3.0</a>, via Wikimedia Commons.
</p>

<blockquote class="information">
    <p>Le pouvoir séparateur de l'oeil s'appelle aussi le pouvoir de <em>résolution</em>. Nous parlerons pas la suite de la <em>résolution d'une image</em> qui ne doit pas être trop petite, au risque de voir cette image pixélisée.</p>
</blockquote>

<div class="a-faire titre" markdown="1">

### ✏️ À faire

L'application proposée sur <a href="http://www.proftnj.com/RGB3.htm" target="_blank">ce site</a> permet d'obtenir différentes couleurs en faisant varier les canaux rouge, vert et bleu (à l'aide des boutons + et - ou en sélectionnant une couleur nommée). Utilisez ce site pour répondre aux questions qui suivent.

</div>

✍️ **Question 5** : Quelles valeurs des canaux (décimal RGB) permettent d'obtenir :
- d'obtenir du rouge (pur) ?
- d'obtenir du blanc ?
- d'obtenir du noir ?
- d'obtenir du jaune (pur) ?
- d'obtenir du cyan ?

✍️ **Question 6** : Que se passe-t-il quand les trois canaux ont la même valeur (par exemple (125,125,125) ou (50, 50, 50)) ?

✍️ **Question 7** : Combien de couleurs différentes est-il possible d’obtenir avec ce système RVB (pour une profondeur de couleur 24 bits comme c'est le cas ici) ? Expliquez.

# Résolution d'une image

<div class="important">
    <p>La <strong>résolution</strong> d'une image est le nombre de pixels par unité de longueur, mesuré communément en <em>pixels par pouce</em> (ppp) à l’écran ou en <em>points par pouce</em> (ppp) pour l’impression. Le <em>pouce</em> est une unité de longueur anglophone, un pouce est égal à 2,54 cm, son symbole est « " ».</p>
    <p>La <strong>taille</strong> d'une image (ou dimension) correspond à ses <em>dimensions physiques</em> (largeur*hauteur) en pouce. On a la relation suivante : $$\text{résolution} = \dfrac{\text{définition}}{\text{taille}}.$$</p>
</div>

**Remarques** :
- Pour une taille fixée, plus la résolution est élevée plus la qualité l'est. Des résolutions trop faibles entraînent un phénomène de pixellisation ;
- <span class="emoji grand">⚠️</span> La *résolution* ne doit pas être confondue avec la *définition* d'une image, ce n'est pas une caractéristique du fichier image. Elle ne peut pas être calculée sans connaître la taille du support. Malheureusement, on retrouve cette confusion quasiment partout... ;
- On rencontre aussi souvent les unités *pixel per inch* (ppi) et *dots per inch* (dpi) qui sont les traductions anglaises de *pixels par pouce* et *points par pouce*.

✍️ **Question 8** : Chacune des figures suivantes représente une image imprimée, un carré représentant un pixel. Pour chaque image, déterminez sa taille (en pouces), sa définition (en pixels) et sa résolution (en ppp).

<img class="centre image-responsive" src="data/image_numerique_figures.png" alt="trois figures">
<p class="legende">
    Source : Extrait du cahier d'activités de SNT de Nathan, page 93.
</p>

✍️ **Question 9** : Repérez les deux imprécisions/erreurs de vocabulaire dans les caractéristiques de l'iPhone 13 données par le site de vente en ligne Cdiscount.

<img class="centre image-responsive" src="data/caracteristiques_iphone13.png" alt="trois figures" width="800">
<p class="legende">
    Source : <a href="https://www.cdiscount.com/telephonie/telephone-mobile/apple-iphone-13-128go-midnight-sans-kit-pieton/f-1440402-app0194252707289.html?awc=6948_1660149657_811b5ecc92aa5d878959b55f0bd05e69&cid=affil&cm_mmc=zanoxpb-_-1087831#desc" target="_blank">Cdiscount</a>.
</p>


# Exercices

## Exercice 1 {.titre-exercice}

1. Mesurez la largeur et la hauteur de l’écran de votre smartphone et convertissez ces mesures en pouces.
2. Cherchez la définition de l’écran de votre smartphone (vous pouvez trouver cette information sur le Web ou dans les paramètres de votre téléphone).
3. Calculez alors la résolution de votre écran en ppp (pixels par pouce). Comparez votre résultat avec les informations trouvées sur le Web.

## Exercice 2 {.titre-exercice}

L'écran d'un smartphone a une résolution de 458 ppp et affiche des images de définition 2436 x 1125. Calculez la taille de cet écran (largeur, hauteur) en cm.

## Exercice 3 {.titre-exercice}

À l'impression, on estime qu’une image est de bonne qualité si sa résolution est supérieure à 300 ppp (points par pouces). Alban a pris une photo dont la définition est 1920 × 2560 pixels. Il souhaite imprimer cette photo dans le plus grand format possible mais que le résultat soit de bonne qualité. 

1. Calculez la résolution de cette image si on l'imprime au format 9 x 12 cm, au format 12 x 16 cm et au format 18 x 24 cm.  
2. Que lui conseillez-vous ?


<div class="a-supprimer" markdown="1">

---

**Références** :
- David Roche, lien vers la séance : [https://pixees.fr/informatiquelycee/n_site/snt_photo_image.html](https://pixees.fr/informatiquelycee/n_site/snt_photo_image.html)
- Article Wikipédia sur le [pouvoir de résolution](https://fr.wikipedia.org/wiki/Pouvoir_de_r%C3%A9solution).
- Manuel de SNT de Bordas.
- Cahier d'activités de SNT de Nathan.

---

Les enseignants de SNT du lycée Mounier, ANGERS

![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>