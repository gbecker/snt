"""
Module permettant d'écrire des programmes pour créer des images en "pixel art"
Adaptation du travail de Mathieu Degrange (https://github.com/DegrangeM/pyxel-art)

Auteur : Germain BECKER
Licence : CC BY-NC-SA

//!\\ ATTENTION
Ce module ne fonctionne qu'avec Basthon et donc aussi sur Capytale.
En effet, il est basé sur le module p5 porté dans Basthon par Romain Casati, module
lui-même basé sur la bibliothèque JavaScript p5.js (qui diffère du module p5 que l'on
peut obtenir avec la commande pip install p5).
"""

from p5 import *
from inspect import currentframe
from copy import deepcopy
import __main__

MARGE_BAS = 50

# Initialisation du dictionnaire stockant toutes les données nécessaires à un pixel art
pixel_art = {
    'L': 9,
    'H': 9,
    'TAILLE_PIXEL': 30,
    'grille_visible': True,
    'coord_visibles': True,
    'correction_visible': False,
    'correction_differee': False,
    'pixels': [[(255, 255, 255)]*9 for j in range(9)],
    'pix_colories': {},
    'etapes': [[(255, 255, 255)]*9 for j in range(9)],
    'num_etape': 0,
    'animation': False,
    'deux_images': False,
    'bons_pixels': [],
    'dessin_libre': False,
    'pixels_proposes': [],
    'programme': ''
}

def dessiner_image(grille, taille_pixel, x=0, y=0):
    """
    Dessine l'image dont les pixels sont représentés par grille.
    
    Paramètres
    ----------
    grille
        une liste de listes (de tuples)
    taille_pixel
        un entier dont la valeur est le côté du carré représentant un pixel
    x
        l'abscisse du coin supérieur gauche de l'image
    y
        l'ordonnée du coin supérieur gauche de l'image
    """
    for i in range(len(grille)):  # numéro de ligne
        for j in range(len(grille[0])):  # numéro de colonne
            if pixel_art['grille_visible']:
                stroke(50)
            # on fixe la couleur du pixel
            fill(grille[i][j])
            # on crée le pixel
            square(x + (j+1) * taille_pixel , y + (i+1) * taille_pixel, taille_pixel)  
            if pixel_art['correction_visible']:
                # on écrit le numéro de ligne du code du script
                ecrire_num_ligne_code(i, j, taille_pixel, x, y)

def ecrire_num_ligne_code(i, j, taille_pixel, x=0, y=0):
    """
    Écrit dans le pixel de coordonnées (j, i) le numéro de ligne du code du 
    script le coloriant.
    """
    textFont("Consolas")
    textSize(taille_pixel // 2 + 1)
    if (j, i) in pixel_art['pix_colories']:
        noStroke()
        fill(255)
        num_ligne = pixel_art['pix_colories'][(j, i)]
        if num_ligne <= 9:
            text(num_ligne, x + (j+1) * taille_pixel + taille_pixel//3 , \
                 y + (i+1) * taille_pixel + 2*taille_pixel//3)
        else:
            text(num_ligne, x + (j+1) * taille_pixel + taille_pixel//4 , \
                 y + (i+1) * taille_pixel + 2*taille_pixel//3)
                    
def ecrire_coordonnees(grille, taille_pixel, x=0, y=0):
    """
    Écrit les valeurs sur les axes des abscisses et des ordonnées.
    
    Paramètres
    ----------
    grille
        une liste de listes (de tuples)
    taille_pixel
        un entier dont la valeur est le côté du carré représentant un pixel
    x
        l'abscisse du coin supérieur gauche de l'image
    y
        l'ordonnée du coin supérieur gauche de l'image
    """
    textSize(taille_pixel // 2 + 1)
    fill(50)
    for i in range(len(grille)):
        if i <= 9:
            text(i, x + taille_pixel//4, y + (i+1) * taille_pixel + 2*taille_pixel//3)
        else:
            text(i, x, y + (i+1) * taille_pixel + 2*taille_pixel//3)
    for j in range(len(grille[0])):
        if j <= 9:
            text(j, x + (j+1) * taille_pixel + taille_pixel//3, y + 2*taille_pixel//3)
        else:
            text(j, x + (j+1) * taille_pixel + taille_pixel//4, y + 2*taille_pixel//3)           

def creer_image(largeur, hauteur):
    """
    Crée une image de dimension largeur * hauteur.
    
    Paramètres
    ----------
    largeur
        un entier positif égal au nombre de pixels en largeur
    hauteur
        un entier positif égal au nombre de pixels en hauteur
    
    Cette fonction permet de réinitialiser toutes les données d'un pixel art.
    """
    if not largeur >= 1:
        raise ValueError("la largeur doit être un entier supérieur à 1.")
    if not hauteur >= 1:
        raise ValueError("la hauteur doit être un entier supérieur à 1.")
    pixel_art['L'] = largeur
    pixel_art['H'] = hauteur
    pixel_art['TAILLE_PIXEL'] = 30
    pixel_art['pixels'] = [[(255, 255, 255)]*largeur for j in range(hauteur)]
    pixel_art['pix_colories'] = {}
    pixel_art['etapes'] = [[[(255, 255, 255)]*largeur for j in range(hauteur)]]
    pixel_art['grille_visible']= True
    pixel_art['coord_visibles'] = True
    pixel_art['correction_visible'] = False
    pixel_art['correction_differee'] = False
    pixel_art['num_etape'] = 0
    pixel_art['animation'] = False
    pixel_art['deux_images'] = False
    pixel_art['bons_pixels'] = []
    pixel_art['dessin_libre'] = False
    #pixel_art['pixels_proposes'] = []

    
def colorier(x, y, couleur=(150,150,150)):
    """
    Modifie la couleur du pixel de coordonnées (x, y).
    Le troisième paramètre couleur est optionnel. Lui donner une valeur pour 
    définir une autre couleur que le gris par défaut.
    """
    if not(0 <= x < pixel_art['L']):
        raise ValueError(f"{x} n'est pas une abscisse valide")
    if not(0 <= y < pixel_art['H']):
        raise ValueError(f"{y} n'est pas une ordonnée valide")
    if not isinstance(couleur, tuple) or len(couleur) != 3:
        raise SyntaxError(("La couleur doit être un triplet (r,v,b) de trois "
                           "valeurs"))
        
    # on colorie le pixel (x, y)
    pixel_art['pixels'][y][x] = couleur
    # on lui associe le numéro de ligne du script qui l'a colorié
    pixel_art['pix_colories'][(x, y)] = currentframe().f_back.f_l