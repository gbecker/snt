Capturer la lumière
=====================

Dans cette séance, nous allons voir comment un appareil photo numérique (et donc aussi un smartphone) fait pour capturer la lumière d'une scène et transformer cela en des données numériques qui serviront à construire l'image finale de pixels.

<img class="centre image-responsive" src="data/selfie.jpg" alt="illustration" width="400"/>
<p class="legende">Crédits : <a href="https://pixabay.com/fr/photos/selfie-photo-cam%c3%a9ra-portrait-858928/" target="_blank">InspiredImages</a>, via Pixabay.</p>

# Diaporama

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/photo_numerique/data/Capturer_la_lumiere.pdf"
    class = "centre"
    width="800"
    height="600"
    style="border: none;">
    </iframe>
</div>
<p class="impression">
    Source diaporama : <a href="https://info-mounier.fr/snt/photo_numerique/data/Capturer_la_lumiere.pdf">https://info-mounier.fr/snt/photo_numerique/data/Capturer_la_lumiere.pdf</a>
</p>

# Questions

✍️ **Question 1** : Avec votre smartphone, prenez une photo de votre table puis cherchez et notez la définition de l'image obtenue (il doit y avoir un outil "informations" lorsque vous ouvrez l'image).

✍️ **Question 2** : Cherchez sur le Web la définition du capteur (principal) de votre smartphone.
- Que représente réellement ce nombre ? 
- Comparez-le à la définition de l'image prise à la question précédente.

✍️ **Question 3** : Voici une animation affichant une image RAW (crue) construite par un appareil photo numérique. Que voit-on apparaître lorsque le zoom est élevé ?

<img class="centre image-responsive" src="data/zoom_image_raw.gif" alt="gif">
<p class="legende">
    <strong>Une image RAW.</strong>
    <br>Crédit : M. Ferrieu (<a href="http://snt.ferrieu.free.fr/22-Ducapteuralaphoto.html" target="_blank">source</a>), d'après une image originale de <a href="https://zestedesavoir.com/articles/3218/votre-appareil-photo-vous-ment-mais-le-contraire-serait-bien-embetant/" target="_blank">SpaceFox</a>, <a href="https://creativecommons.org/licenses/by/4.0/deed.fr" target="_blank">CC BY 4.0</a> .
</p>

✍️ **Question 4** : Faites un schéma bilan qui résume les différentes étapes permettant à un APN de créer une telle image RAW à partir d'une scène lumineuse. *Le schéma devra faire apparaître les différents éléments de l'APN et leurs rôles*.

# Bilan

## Vidéo

▶️ Si nécessaire, voici une vidéo qui reprend beaucoup d’éléments de cette séance (**jusqu’à 9’00"**) :

<div class="video-responsive">
    <div class="youtube_player centre" videoID="Rs5ab3X9Oxo" end="540" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/Rs5ab3X9Oxo">https://youtu.be/Rs5ab3X9Oxo</a>
</p>

<div class="a-supprimer" markdown="1">

---
**Références**
- Les références pour le diaporama sont indiquées en dernière page de celui-ci.
- Cours de SNT <a href="http://snt.ferrieu.free.fr/22-Ducapteuralaphoto.html" target="_blank"><em>Du capteur à l'image</em></a> de M. Ferrieu, pour le GIF de l'image RAW.

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>
