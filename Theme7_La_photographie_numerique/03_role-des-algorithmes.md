Des algorithmes pour fabriquer l'image finale
=============================================

Dans cette séance, nous allons voir que lorsque l'on prend une photo avec notre appareil, de nombreux algorithmes sont appliqués pour obtenir l'image finale, celle qui s'enregistre sur notre carte mémoire. Sans ces algorithmes, les photos seraient de très mauvaise qualité !

<img class="centre image-responsive" src="data/mobile-photography.jpg" alt="illustration" width="400"/>
<p class="legende">Crédit : <a href="https://pixabay.com/fr/photos/photographie-mobile-4210044/" target="_blank">broamer_merta</a>, via Pixabay.</p>

# Diaporama

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/photo_numerique/data/Role_des_algorithmes.pdf"
    class = "centre"
    width="800"
    height="600"
    style="border: none;">
    </iframe>
</div>
<p class="impression">
    Source diaporama : <a href="https://info-mounier.fr/snt/photo_numerique/data/Role_des_algorithmes.pdf">https://info-mounier.fr/snt/photo_numerique/data/Role_des_algorithmes.pdf</a>
</p>

# Questions

✍️ **Question 1** : Associez chacun des algorithmes suivants à sa description :

- HDR (High Dynamic Range)
- Balance des blancs
- Correction de l'exposition
- Focus-stacking (empilement de mises au point)
- Stabilisation optique
- Focus-peaking (aide à la mise au point)

**Algorithme 1**

```
Si un mouvement est détecté par le gyroscope ou l'accéléromètre, alors
    déplacer le support du capteur dans le sens opposé au mouvement
```

**Algorithme 2**

```
Chercher le pixel le plus clair de l'image
Mettre les trois composantes RVB de ce pixel à 255
Corriger la valeur de tous les autres pixels proportionnellement
```

**Algorithme 3**

```
Pour chaque pixel,
    Pour chaque composante R, V et B de ce pixel
        Si sa valeur est inférieure ou égale à 235 alors
            lui ajouter 20
        sinon
            lui attribuer la valeur 255
```

**Algorithme 4**

```
Prendre plusieurs photographies avec des expositions différentes.
Créer une image en fusionnant les pixels des zones les mieux exposées
selon les différentes prises de vue.
```

**Algorithme 5**

```
Pour chaque pixel,
    Si le constraste avec un pixel voisin est fort, alors
        colorier le pixel en rouge sur l'écran de l'appareil
```

**Algorithme 6**

```
Prendre plusieurs photographies avec des mises au point différentes.
Créer une image en fusionnant les pixels des parties les plus nettes 
de chaque prise de vue.
```

✍️ **Question 2** : Lors du dématriçage, pour obtenir une image ayant autant de pixels qu'il n'y a de photosites dans le capteur, chaque photosite doit donner un pixel. Une possibilité est de calculer les deux composantes RVB manquantes en faisant la moyenne des valeurs de ses voisins directs : par exemple, si le photosite correspond à un filtre bleu, alors sa composante bleue est connue et on calcule ses composantes rouge et verte à partir des valeurs rouge et verte des voisins directs.
1. En utilisant cette méthode, calculez les composantes R et V du pixel central dans la situation ci-dessous.

<img class="centre image-responsive" src="data/photosite1.png" alt="des photosites" width="400"/>

2. Calculez les composantes manquantes du pixel central dans la situation ci-dessous.

<img class="centre image-responsive" src="data/photosite2.png" alt="des photosites" width="400"/>

<blockquote class="information">
    <p>On rappelle qu'en pratique, les algorithmes de dématriçage utilisés sont bien plus complexes que cela, l'idée ici était juste de comprendre le principe.</p>
</blockquote>

✍️ **Question 3** : Prenez une photo de votre table avec votre téléphone puis cherchez les réponses aux questions suivantes :
1. Quelle est la définition de cette image ?
2. Quelle est le format (l'extension) de cette image ?
2. Quelle est la taille (en Mo) du fichier image.

✍️ **Question 4** : Cette photo est une image dont la profondeur de couleur est 24 bits, ce qui signifie que chaque pixel est théoriquement codé sur 3 octets. 
1. Calculez la taille *théorique*, en octet, du fichier image brut non compressé.
2. En comparant avec la taille *réelle* du fichier image (question précédente), expliquez pourquoi l'image sauvegardée sur votre téléphone est compressée.
3. Calculez le rapport $\dfrac{\text{taille réelle}}{\text{taille théorique}}$. Interprétez le résultat et indiquez le gain de place, en pourcentage, permis par cette compression.

✍️ **Question 5** : Faites un schéma bilan qui résume les différentes étapes permettant à un APN de créer une image. Pour cela, vous organiserez de manière chronologique les étapes suivantes, en veillant à ajouter des informations essentielles pour chacune d'elle :
- Capture de la lumière par le capteur photo
- Dématriçage
- Algorithmes de développement de l'image
- Enregistrement de l'image brute (RAW)
- Enregistrement sur la carte mémoire
- Algorithmes de prise de vue
- Algorithme de compression de l'image

# Bilan

## Vidéos

Si nécessaire, voici une vidéo qui reprend l’étape du dématriçage (**à partir de 9’01"**) :

<div class="video-responsive">
    <div class="youtube_player centre" videoID="Rs5ab3X9Oxo" start="541" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/Rs5ab3X9Oxo?t=541">https://youtu.be/Rs5ab3X9Oxo?t=541</a>
</p>

Et voici une vidéo qui reprend une partie des algorithmes mis en oeuvre lors de la création d’une image :

<div class="video-responsive">
    <div class="youtube_player centre" videoID="fW9bXLCooAE" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/fW9bXLCooAE">https://youtu.be/fW9bXLCooAE</a>
</p>


<div class="a-supprimer" markdown="1">

---
**Références**
- Les références pour le diaporama sont indiquées en dernière page de celui-ci.
- Chaîne YouTube de [Monsieur Techno](https://www.youtube.com/channel/UCKpQL4XedlNqInF6AORB90Q) pour les deux vidéos de résumé sur le fonctionnement d'un appareil photo numérique.

---
Les enseignants de SNT du lycée Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>