Découverte des notions de filtre et de tri
==========================================

Lorsqu'on dispose de données numériques structurées, deux opérations usuelles consistent à **filtrer** et à **trier** les données. Par exemple, sur un site de vente en ligne :
- on peut *filtrer* les articles selon le fabricant : cela permet de n'afficher que les articles du fabricant sélectionné
- on peut *trier* les articles par ordre croissant/décroissant de prix

Dans cette activité, vous allez découvrir ces notions de *filtre* et de *tri*.

# Activité

<img class="centre image-responsive" src="data/castor_medium.png" alt="logo castor">

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Rendez-vous sur la plateforme <a href="https://concours.castor-informatique.fr/" target="_blank">https://concours.castor-informatique.fr/</a> puis saisissez le code fourni par votre enseignant.

⚠️ **Attention** : Vous saisirez votre prénom et la première lettre de votre nom de famille. N'oubliez pas de noter le code d'accès personnel qui vous sera donné avant de commencer l'épreuve (il vous permettra de faire les questions en plusieurs fois).

</div>

# Bilan

✍️ **Question** : Donnez un exemple concret de votre vie numérique au cours duquel vous pouvez effectuer une opération de filtre. Même question pour une opération de tri.

---
Les enseignants du lycée Emmanuel Mounier à Angers 

<img class="centre image-responsive" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" alt="Licence Creative Commons">

