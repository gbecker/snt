Filtrer et trier des données avec un tableur
============================================

Dans cette séance, vous allez filtrer et trier des données avec un tableur. La première activité est obligatoire, la seconde est optionnelle.

# Activité 1 : Morceaux de musique

<div class="a-faire titre" markdown="1">

### ✏️ À faire

▶️ Regardez la vidéo suivante (5 min) qui explique comment filtrer et trier des données avec le tableur *Calc* de *LibreOffice*.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="kSnQVozzeTc" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/kSnQVozzeTc">https://youtu.be/kSnQVozzeTc</a>
</p>

💻 Rendez-vous ensuite sur la plateforme <a href="https://concours.castor-informatique.fr/" target="_blank">https://concours.castor-informatique.fr/</a> puis saisissez le code fourni par votre enseignant.  
Effectuez le parcours proposé pour filtrer et trier des morceaux de musique dont les données sont stockées dans un fichier CSV.

<blockquote markdown="1">

⚠️ **Attention** : Vous saisirez votre prénom et la première lettre de votre nom de famille. N'oubliez pas de noter le code d'accès personnel qui vous sera donné avant de commencer l'épreuve (il vous permettra de faire les questions en plusieurs fois).

</blockquote>

</div>

# Activité 2 : Les prénoms donnés à Angers (optionnelle)

## Récupérer les données sur un site de données ouvertes

<div class="a-faire titre" markdown="1">

### ✏️ À faire

<img class="image-responsive centre" src="data/logo_data_angers.png" alt="logo du site data.angers.fr" width="400">

La ville d’Angers met à disposition des données ouvertes sur le site <a href="https://data.angers.fr/pages/home/" target="_blank">https://data.angers.fr/pages/home/</a>.

Ouvrez cette page Web et tapez dans la barre de recherche « prénoms des enfants déclarés à Angers », validez puis cliquez sur le jeu de données portant ce nom (vous devriez alors arriver sur la page https://data.angers.fr/explore/dataset/prenoms-des-enfants-nes-a-angers/information/).

Répondez aux questions suivantes, en utilisant notamment *les informations disponibles via les onglets « Informations » et « Tableau »*.

</div>

✍️ **Question 1** : Quels sont les noms des descripteurs (ou *en-têtes*) de cette table ?

✍️ **Question 2** : Combien d’objets (ou enregistrements) contient cette table de données ?

✍️ **Question 3** : Quel est le critère pour qu’un prénom soit anonymisé (et n’apparaisse alors pas dans le jeu de données) ? Pourquoi à votre avis ?

✍️ **Question 4** : Allez dans l’onglet « Export » puis télécharger le fichier de données au format CSV puis ouvrez ce fichier avec un éditeur de texte pour vérifier le caractère de séparation utilisé. Quel est ce caractère de séparation ?

<blockquote class="information" markdown="1">

Vous remarquerez que le jeu de données sur le prénoms est également disponible au format JSON (voir séance 1), mais nous travaillerons uniquement avec le format CSV.

</blockquote>

## Traiter les données avec un tableur

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Ouvrez le fichier *prenoms-des-enfants-nes-a-angers.csv* (téléchargé à la question précédente) avec le tableur Calc de LibreOffice. Pour cela : cliquez droit sur le fichier puis sur « Ouvrir avec » et enfin sur « LibreOffice » (ou « LibreOffice Calc »).

⚠️ **Attention** : à l’ouverture, vous vérifierez que :
- le bon caractère de séparation est coché (celui déterminé à la question 5).
- le Jeu de caractères sélectionné soit « Unicode (UTF-8) » (évite les problèmes avec les accents)

Vous devez obtenir un écran similaire à celui-ci-dessous.

<img class="centre image-responsive" src="data/capture_ecran_prenoms.png" alt="capture d'écran">

**Utilisez le tableur pour répondre aux questions qui suivent.**

</div>

### Filtrer et trier des données

<blockquote class="attention" markdown="1">

**Attention** : N’oubliez pas d’annuler tous les filtres et tris avant de commencer chaque question.

</blockquote>

💻 **Question 5** : Filtrez les données de manière à afficher uniquement les prénoms de l’année 2022. Quel est le dernier prénom affiché ?

> Utilisez le « Filtre standard » (onglet Données > Plus de filtres > Filtre standard) pour répondre aux trois questions qui suivent :

💻 **Question 6** : Filtrez les données de manière à n’afficher que les filles dont le prénom a été donné plus de 30 fois en 2022. Quels sont les premier et dernier prénom de ce filtre ? 

💻 **Question 7** : Filtrez les données pour n’afficher que les prénoms de l’année 2013 puis triez ces prénoms du plus donné au moins donné (par nombre d’occurrence décroissant). Quels sont les 3 prénoms les plus donnés en 2013 ?

💻 **Question 8** : On souhaite afficher à l’écran uniquement les prénoms féminins commençant par la lettre « C », triés par ordre alphabétique, qui ont été donnés plus de 20 fois entre 2019 et 2022. Quel est le dernier prénom affiché ?

### Visualiser graphiquement des données

<div class="a-faire titre" markdown="1">

### ✏️ À faire

▶️ Regardez la vidéo suivante (3 min) qui explique comment représenter graphiquement des données avec un tableur (*Calc* de *LibreOffice*), puis répondez aux questions qui suivent.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="xFsgj0EmtFo" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    ▶️ Source : <a href="https://youtu.be/xFsgj0EmtFo">https://youtu.be/xFsgj0EmtFo</a>
</p>

</div>

<blockquote class="attention" markdown="1">

**Attention** : N’oubliez pas d’annuler tous les filtres et tris avant de commencer chaque question.

</blockquote>

💻 **Question 9** : Filtrez les données pour afficher uniquement les données sur le prénom « Théo ». Triez ensuite le résultat par années croissantes et affichez un diagramme en bâtons permettant de visualiser graphiquement l’évolution du nombre d’occurrences de ce prénom.

💻 **Question 10** : Faites-en de même avec votre prénom si celui-ci est présent dans
les données, sinon avec le prénom d’un de vos camarades.

💻 **Question 11 (défi)** : En utilisant les outils de filtre et de tri, réalisez le diagramme du Top 15 des prénoms donnés en 2022 à Angers. Vous devez obtenir un diagramme similaire à celui ci-dessous (qui correspond à l'année 2020).

<img class="centre image-responsive" src="data/top15_prenoms_2020.png" alt="diagramme du top 15 des prénoms en 2020" width="450">

---
Les enseignants du lycée Emmanuel Mounier à Angers 

<img class="centre image-responsive" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" alt="Licence Creative Commons">
