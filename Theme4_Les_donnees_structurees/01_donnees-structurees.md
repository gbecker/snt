# Structurer des données

# Définitions

Commençons par quelques définitions !

<blockquote class="information">
    <p>Qu'est ce qu'une donnée ? Qu'est-ce qu'une donnée personnelle ?</p>
</blockquote>

<div class="important">
    <p>Une <strong>donnée</strong> est une valeur ou une information décrivant un objet, une personne, un événement, etc.</p>
    <p>Une <strong>donnée personnelle</strong> est toute information se rapportant à une personne physique identifiée ou identifiable.</p>
</div>

Il est important de noter qu'une personne physique peut être identifiée :
- de manière directe (exemple : nom et prénom)
- de manière indirecte (exemple : par un numéro de téléphone, une plaque d'immatriculation, etc.)

<blockquote class="information">
    <p>Avez-vous d'autres exemples de données personnelles qui permettent d'identifier indirectement une personne physique ?</p>
</blockquote>

Notez que l'identification d'une personne physique peut être réalisée :
- à partir d'une seule donnée
- à partir du croisement d'un ensemble de données (exemple : une femme vivant à telle adresse et membre dans telle association)

# Le format CSV

## Pour structurer des données

Le format CSV est très courant sur le Web. Voici ce que nous dit Wikipédia sur le format CSV :

<blockquote class="citation">
    <p>Comma-separated values, connu sous le sigle CSV, est un format informatique ouvert représentant des données tabulaires sous forme de valeurs séparées par des virgules.
    <br>Un fichier CSV est un fichier texte, par opposition aux formats dits « binaires ». Chaque ligne du texte correspond à une ligne du tableau et les virgules correspondent aux séparations entre les colonnes. Les portions de texte séparées par une virgule correspondent ainsi aux contenus des cellules du tableau.</p>
</blockquote>

Voici un exemple du contenu d'un fichier CSV :

```csv
nom,prenom,date_naissance
Johnson,Katherine,26/08/1918
Lovelace,Ada,10/12/1815
Hamilton,Margaret,17/08/1936
```

Je pense qu'il est évident pour vous que nous avons ici 3 femmes :

* Katherine Johnson qui est née le 26/08/1918
* Ada Lovelace qui est née le 10/12/1815
* Margaret Hamilton qui est née le 17/08/1936

Dans ce fichier :

* Les éléments sur la première ligne, à savoir `nom`, `prenom` et `date_naissance`, sont appelés des **descripteurs**. Ces descripteurs permettent de décrire les données en définissant leur contenu.
* `Johnson`, `Lovelace` et `Hamilton` sont les **valeurs** du descripteur `nom`.
* Chacune des lignes de ce fichier (hormis la première qui contient les descripteurs) s'appelle un **objet** ou un **enregistrement**. Ce fichier possède donc 3 enregistrements (ou 3 objets).
* Un regroupement d'objets (ou personnes, etc.) partageant les mêmes descripteurs s’appelle une **collection** de données. Le tableau ci-dessus présente donc une collection sur des femmes scientifiques célèbres.

✍️ **Question 1** : Donnez les différentes valeurs du descripteur `date_naissance` du fichier précédent.

✍️ **Question 2** :  Choisissez l'une des trois personnalités de ce fichier puis :

*   résumez en une phrase de qui il s'agit ,
*   et donnez le lien vers sa page Wikipedia.

<blockquote class="attention">
    <p>La virgule est un standard pour les données anglo-saxonnes, mais pas pour les données aux normes françaises. En effet, en français, la virgule est le séparateur des chiffres décimaux. Il serait impossible de différencier les virgules des décimaux et les virgules de séparation des informations. C’est pourquoi on utilise souvent, en France, un autre séparateur : le point-virgule (;). Dans certains cas cela peut engendrer quelques problèmes, vous devrez donc rester vigilants sur le type de séparateur utilisé.</p>
</blockquote>

## Ouverture d'un fichier CSV

Comme un fichier CSV est un fichier texte, on peut le visualiser avec n'importe quel éditeur de texte (capture d'écran ci-dessous).

<img class="centre image-responsive" src="data/bloc_note.png" alt="fichier CSV ouvert avec un éditeur de texte">

<p class="legende"><strong>Fichier CSV ouvert avec un éditeur de texte</strong></p>

Mais comme un fichier CSV représente des données tabulaires, on peut représenter ces données dans un tableau qui s'appelle une **table de données** :

| nom | prenom | date_naissance |
| --- | --- | --- |
| Johnson | Katherine | 26/08/1918 |
| Lovelace | Ada | 10/12/1815 |
| Hamilton | Margaret | 17/08/1936 |

Et à ce titre, on peut l'ouvrir avec n'importe quel tableur (capture d'écran ci-dessous) qui range les données du fichier CSV dans un tableau avec des lignes et des colonnes :

<img class="centre image-responsive" src="data/tableur.png" alt="fichier CSV ouvert avec un tableur">
<p class="legende"><strong>Fichier CSV ouvert avec le tableur Calc de LibreOffice</strong></p>

💻 **Question 3** : Téléchargez ce fichier CSV en cliquant sur le lien suivant : <a href="data/personnalites.csv" target="_blank" download>personnalites.csv</a>. Puis ouvrez-le de deux manières :
- **avec un éditeur de texte** : clic droit sur le fichier téléchargé puis "Ouvrir avec" et sélectionnez "Bloc-notes"
- **avec un tableur** : clic droit sur le fichier téléchargé puis "Ouvrir avec" et sélectionnez "LibreOffice" puis ***sélectionnez la virgule comme séparateur*** lors de l'ouverture.
Vous devriez obtenir des écrans similaires à ceux donnés au-dessus.

💻 **Question 4** : Fermer le tableur puis, dans l'éditeur de texte, ajoutez une ligne (un objet) pour la graphiste <a href="https://fr.wikipedia.org/wiki/Susan_Kare" target="_blank">Susan Kare</a> et enregistrez les modifications. Vérifiez en ouvrant le fichier avec le tableur, que la ligne a bien été ajoutée dans le tableau.

# Exercices

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 1 : Télécharger un fichier CSV de données ouvertes

<!-- ## Exercice 1 : Télécharger un fichier CSV de données ouvertes  {.titre-exercice} -->

<img class="centre image-responsive" src="data/logo_data_angers.png" alt="logo data.angers.fr" width="400">
<p class="legende">Source : <a href="https://data.angers.fr/pages/home/">https://data.angers.fr/pages/home/</a></p>

La ville d'Angers met à disposition des citoyens les données en temps réel sur les disponibilités dans les parkings angevins (il y a plein d'autres jeux de données).

Ces données sont accessibles via cette page <a href="https://data.angers.fr/explore/dataset/parking-angers/information/" target="_blank">https://data.angers.fr/explore/dataset/parking-angers/information/</a>.

💻 **Question 1** : Télécharger ces données au format CSV en allant dans l'onglet *Export* de cette page.

✍️ **Question 2** : Ouvrez le fichier téléchargé avec un éditeur de texte et répondez aux questions suivantes :
1. Quel est le *caractère de séparation* utilisé dans ce jeu de données ?
2. Quels sont les *descripteurs* de ce jeu de données ? (revoir la définition dans le paragraphe précédent si besoin).
3. Combien d'*enregistrements* possède ce jeu de données ? (revoir la définition dans le paragraphe précédent si besoin).

<blockquote markdown="1">

N'hésitez pas à ouvrir également ce fichier avec un tableur en veillant à sélectionner le bon caractère de séparation !  

</blockquote>

</div>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 2 : Les vidéos YouTube

Dans cette activité, on considère un objet particulier : une **vidéo YouTube**.

Les vidéos YouTube et toutes leurs caractéristiques sont enregistrées de manière **structurée** sur les serveurs de YouTube (ce n'est pas dans des fichiers CSV mais dans ce qu'on appelle des bases de données, mais le principe est similaire). C'est ce qui permet de rechercher des vidéos à partir de mots clés et d'afficher les résultats comme sur la capture d'écran ci-dessous. 

<img class="centre image-responsive" src="data/chatgpt_yt.png" alt="résultats d'une recherche YouTube" width="900">
<p class="legende"><strong>Capture d'écran des premiers résultats YouTube pour le mot clé <em>chatgpt</em></strong>.</p>

✍️ **Question 1** : Vous devez constater que les données affichées en résultat ont la même forme quelle que soit la vidéo. Identifiez tous les descripteurs des vidéos YouTube qui apparaissent à l'écran.

✍️ **Question 2** : Construisez une table de données avec les descripteurs sur la première ligne et leurs valeurs pour l'une des 4 vidéos (celle de votre choix).

</div>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 3 : Formats CSV et JSON

Dans cet exercice on considère la table de données suivante :

<!-- <table class="first-line-only">
    <tr>
        <th>nom</th>
        <th>prénom</th>
        <th>classe</th>
    </tr>
    <tr>
        <td>Dubois</td>
        <td>Gabriel</td>
        <td>2C</td>
    </tr>
    <tr>
        <td>Marchand</td>
        <td>Élodie</td>
        <td>1G1</td>
    </tr>
</table> -->

| nom | prénom | classe |
| --- | --- | --- |
| Dubois | Gabriel | 2C |
| Marchand | Élodie | 1G1 |

Pour mémoriser cette table de manière permanente dans un ordinateur, il faut la stocker dans un fichier. L'activité propose de travailler sur deux formats très utilisés : le format CSV (abordé plus haut) et le format JSON (JavaScript Object Notation). Voici la table écrite dans deux fichiers au format CSV puis au format JSON :

**Données au format CSV**

```
nom,prénom,classe
Dubois,Gabriel,2C
Marchand,Élodie,1G1
```

**Données au format JSON**

```json
[
    {
        "nom": "Dubois",
        "prénom": "Gabriel",
        "classe": "2C"
    },
    {
        "nom": "Marchand",
        "prénom": "Élodie",
        "classe": "1G1"
    }
]
```

<blockquote class="information">
    <p>Un fichier JSON est aussi un format de données textuelles. Ce format permet de représenter des données plus complexes que des tables (et donc plus complexes qu'avec un fichier CSV). C'est un format très utilisé sur le Web pour récupérer et échanger des données</p>
</blockquote>

✍️ **Question 1** : Quels sont les objets de cette table ? Quels sont les descripteurs ?

✍️ **Question 2** : Dans le format CSV, quel est le caractère permettant de séparer les données de chaque colonne ?

✍️ **Question 3** : Écrivez le contenu des fichiers CSV et JSON correspondant à la table ci-dessous :

| Date | Equipe 1 | Equipe 2 | Score |
| :---  | :--- | :---  | :--- |
| 10/02/2022 | Angers | Paris | 7-0 |
| 02/03/2020 | Nantes | Angers | 2-8 |

✍️ **Question 4** : Construisez la **table** correspondant au fichier CSV suivant :

```csv
nom_commune,code_commune,pop2007,pop2012
BRIOLLAY,49048,2565,2751
VILLEVEQUE,49377,2738,2858
ECOUFLANT,49129,3747,3775
```

</div>

# Bilan

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Complétez les pointillés par les mots suivants :  
donnée, structurées, CSV, collection, table, descripteurs, ligne, colonne

</div>

* Une ........................... est une valeur décrivant un objet, une personne, un événement digne d’intérêt pour celui qui choisit de la conserver. 
* Plusieurs ................................... peuvent être utiles pour décrire un même objet (par exemple des descripteurs permettant de caractériser un contact : nom, prénom, adresse et numéro de téléphone). 
* Une ................................... regroupe des objets partageant les mêmes descripteurs (par exemple, la collection des contacts d’un carnet d’adresses). 
* La structure de ................................... permet de présenter une collection : les objets en ................................... les descripteurs en ................................... et les données à l’intersection. Les données sont alors dites ...................................
* Pour assurer la persistance des données, ces dernières sont stockées dans des fichiers. Le format ................................... (Comma Separated Values, les données avec des séparateurs) est un format de fichier simple permettant d’enregistrer une table. Il en existe d’autres, comme le format JSON.
* Structurer des données permet d’effectuer des opérations très rapidement sur ces données (rechercher, filtrer, trier, calculer), ce qui sera vu par la suite.

---
**Références** :
- Cours de David Roche sur les [différents types de données structurées](https://dav74.github.io/site_snt/a17/)
- CNIL pour la définition d'une donnée personnelle : [https://www.cnil.fr/fr/definition/donnee-personnelle](https://www.cnil.fr/fr/definition/donnee-personnelle)

---
Les enseignants du lycée Emmanuel Mounier à Angers 

<img class="centre image-responsive" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" alt="Licence Creative Commons">

			
			


