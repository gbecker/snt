Séance d'introduction - SNT
===========================

# Présentation de la matière

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/data/Presentation_SNT.pdf"
    class = "centre"
    width="800"
    height="600"
    style="border: none;">
    </iframe>
</div>

<p class="impression">
Lien du diaporama : <a href="https://info-mounier.fr/snt/data/Presentation_SNT.pdf">https://info-mounier.fr/snt/data/Presentation_SNT.pdf</a>
</p>

# Première activité

<img class="centre image-responsive" src="https://upload.wikimedia.org/wikipedia/commons/e/e5/NASA_logo.svg" alt="logo de la NASA" width="300">
<p class="legende">
    Crédit : <a href="https://commons.wikimedia.org/wiki/File:NASA_logo.svg" target="_blank">NASA</a>, Domaine public, via Wikimedia Commons
</p>

Le robot *Perseverance* s'est posé sur Mars le 18 février 2021. Une très belle prouesse technologique et scientifique qui n'aurait évidemment pas été possible sans informatique.

La vidéo ci-dessous montre l'arrivée sur Mars de Perseverance :

<div class="video-responsive">
    <!-- <iframe class="centre" src="https://ladigitale.dev/digiplay/inc/video.php?videoId=7S3VI6ryfxY&vignette=https://i.ytimg.com/vi/7S3VI6ryfxY/hqdefault.jpg&debut=0&fin=47&largeur=200&hauteur=113" allowfullscreen frameborder="0" width="700" height="394"></iframe> -->
    <div class="video-responsive">
        <div class="youtube_player centre" videoID="7S3VI6ryfxY" start="0" end="47" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
    </div>
</div>
<p class="impression">
    Source : <a href="https://ladigitale.dev/digiplay/#/v/63077f65a8c32">https://ladigitale.dev/digiplay/#/v/63077f65a8c32</a>
</p>

Certains ont trouvé les motifs du parachute du robot assez étranges, et pour cause : un message y a été caché par la NASA !

<div class="a-faire" markdown="1">

**Objectif**

Votre objectif est de découvrir (une partie de) ce message caché !

</div>

Plus précisément, vous devez découvrir la phrase qui se cache dans la couronne intérieure du parachute.

Pour vous aider, vous trouverez tout d'abord ci-dessous un schéma du parachute. La phrase à deviner démarre au niveau du secteur sur lequel est inscrit la lettre D qui est effectivement la signification de ce secteur. Les autres secteurs sont marqués d’un point d’interrogation et la phrase mystère s’obtiendra en tournant en spirale depuis la lettre D, dans le sens horaire :

<img class="centre image-responsive" src="data/parachute.png" alt="schéma du parachute">
<p class="legende">
    <strong>Couronne intérieure du parachute.</strong>
</p>

Et voici un document avec des indices à bien analyser : <a href="data/indices_parachute_perseverance.pdf" target="_blank">https://info-mounier.fr/snt/data/indices_parachute_perseverance.pdf</a>.

**<span class="emoji grand">🕵🏼‍♂️</span> Trouverez-vous cette phrase ? <span class="emoji grand">🕵🏼‍♀️</span>**

# Le langage binaire

Avec le message caché dans le parachute de Perseverance on vient de voir comment on peut numériser de l'information. En l'occurrence, comment remplacer une phrase par une série de nombres (représentés par deux couleurs dans le cas du parachute). En réalité, toutes les informations manipulées par un ordinateur doivent être numérisées, pour pouvoir être écrites comme une série de 0 et de 1. Voyons pourquoi tout de suite !

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/data/langage_binaire.pdf"
    class = "centre"
    width="800"
    height="600"
    style="border: none;">
    </iframe>
</div>
<p class="impression">
Lien du diaporama : <a href="https://info-mounier.fr/snt/data/langage_binaire.pdf">https://info-mounier.fr/snt/data/langage_binaire.pdf</a>
</p>

---
Les enseignants du lycée Emmanuel Mounier à ANGERS
![Licence Creative Commons BY-SA](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

