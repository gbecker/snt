L'histoire d'Internet
=====================

Dans cette séance nous allons aborder l'histoire de la création d'Internet, une histoire qui se rélève à la fois scientifique, militaire, culturelle et politique. Les grandes lignes de la naissance d'Internet sont présentées dans un diaporama, suivi d'un quiz pour vérifier si vous avez retenu l'essentiel.

<img class="centre image-responsive" src="data/Arpanet_1974.svg" alt="illustration" width=""/>
<p class="legende">
    <strong>Le réseau ARPANET en 1974, un ancêtre d'Internet.</strong>
    <br>Source : <a href="https://fr.wikipedia.org/wiki/ARPANET#/media/Fichier:Arpanet_1974.svg">Wikipedia</a>, domaine public.
</p>

# Une histoire scientifique, militaire, culturelle et politique

<div class="pdf-responsive">
    <iframe src="/pdfjs-dist/web/viewer.html?file=/snt/internet/data/Internet_histoire.pdf"
    class = "centre"
    width="800"
    height="600"
    style="border: none;">
    </iframe>
</div>
<p class="impression">
    Source diaporama : <a href="https://info-mounier.fr/snt/internet/data/Internet_histoire.pdf">https://info-mounier.fr/snt/internet/data/Internet_histoire.pdf</a>
</p>

<div class="a-supprimer">

---
Les enseignants de SNT du lycée Emmanuel Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>