# Communication entre deux machines : protocole TCP/IP

Les ordinateurs du réseau Internet utilisent le protocole TCP/IP pour communiquer entre eux. Nous étudierons le fonctionnement de ce(s) protocole(s), après avoir défini quelques termes importants du jargon d'Internet.

# Partie 1 : S'approprier le vocabulaire important

Voici un schéma représentant une version miniature et simplifiée d'Internet.

<img class="centre image-responsive" src="data/mini_internet.png" alt="un mini réseau Internet">

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Après avoir lu attentivement ce qui suit, identifiez sur ce schéma du réseau :
- les switchs ;
- les différents réseaux locaux, que vous entourerez ;
- les différents routeurs, que vous listerez.

</div>

Les **réseaux locaux** sont des (petits) réseaux constitués d'un ensemble de machines à l'échelle d'une maison, d'une entreprise, d'un lycée, etc. Les machines d'un même réseau local peuvent communiquer directement entre elles.

Le plus simple pour relier un ensemble de machines dans un même réseau local est d'utiliser un **switch** (on dit **commutateur** en français), qui est un équipement informatique sur lequel on relie toutes les machines d'un réseau local grâce à des prises RJ45 femelles dans lesquelles on peut brancher des câbles Ethernet, aussi appelés “câbles réseau”. Voici deux photos de switchs :

<img class="centre image-responsive" src="data/switch1.jpg" alt="un switch">
<p class="legende">
    <strong>Un switch</strong>
    <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Switch_reseau.jpg" target="_blank">KoS</a>, Domaine public, via Wikimedia Commons.
</p>

<img class="centre image-responsive" src="data/switch2.jpg" alt="un switch" width="50%">

<p class="legende">
    <strong>Switch!</strong>
    <br>par <a target="_blank" rel="noopener noreferrer" href="https://www.flickr.com/photos/83711730@N06">Andrew Hart</a>, <a target="_blank" rel="noopener noreferrer" href="https://creativecommons.org/licenses/by-sa/2.0/?ref=openverse">CC BY-SA 2.0</a>, via Flickr.
</p>

Chaque réseau local peut être relié à Internet s'il est connecté à un **routeur**. Les routeurs sont des machines qui connectent deux ou plusieurs réseaux (locaux) et qui sont chargés de guider les messages échangés (on parle de paquets) à travers le réseau Internet. Ainsi, si une machine d'un réseau local veut communiquer avec une machine d'un autre réseau local, elle va lui envoyer un message qui va passer de routeur en routeur. Lorsqu'un routeur reçoit un message, il décide grâce à des algorithmes à qui l'envoyer pour qu'il se rapproche du destinataire. Ce processus s'appelle le **routage des paquets**.

<blockquote class="information">
    <p>Lorsqu'avec votre ordinateur (ou smartphone) vous consultez une page Web, l'ordinateur doit envoyer un message à la machine (un serveur dans ce cas) qui stocke la page Web. Cette machine n'appartient pas au même réseau local donc il est routé à travers Internet jusqu'à arriver à destination. Nous étudierons cela dans le thème sur le Web.</p>
</blockquote>

Voici une photo d'un routeur :

<img class="centre image-responsive" src="data/routeur.jpg" alt="un routeur" width="400">
<p class="legende">
    <strong>Routeur Cisco CRS-1 (2004)</strong>
    <br>Crédit : Photo fournie par Cisco Systems Inc., <a href="http://creativecommons.org/licenses/by-sa/3.0/" title="Creative Commons Attribution-Share Alike 3.0">CC BY-SA 3.0</a>, via <a href="https://commons.wikimedia.org/w/index.php?curid=2621637">Wikimedia Commons</a></a>.
</p>

Ainsi, on peut donc voir la structure du réseau Internet comme un immense réseau constitué d'un ensemble de petits réseaux interconnectés les uns aux autres par des routeurs. C'est pour cela que l'on dit qu'**Internet est le réseau des réseaux**.

Pour que deux machines puissent s'échanger des messages elles doivent respecter un **protocole de communication**, c'est-à-dire un ensemble de règles précisant :
- le format des informations échangées, 
- la manière de les échanger, 
- la manière d'établir la communication et de la terminer. 

Dans le cas d'Internet, cette communication entre deux machines se fait selon le **protocole TCP/IP** qui sera abordé dans la partie suivante.

# Partie 2 : Jouons à Internet pour comprendre le protocole TCP/IP

L'objectif de cette partie est de comprendre de manière débranchée le fonctionnement du protocole TCP/IP, c'est-à-dire la manière dont deux machines communiquent sur le réseau Internet.

Pour ouvrir le diaporama en pleine page, <a href="https://acnantesfr-my.sharepoint.com/:p:/g/personal/germain_becker_ac-nantes_fr/ERepctxhM4BHt3L-dtIrJZoBPJrDt3L7-DP9iltqgCYbBw?e=31A7hd" target="_blank">cliquez ici</a> !

<div class="video-responsive">
    <iframe class="centre" src="https://acnantesfr-my.sharepoint.com/:p:/g/personal/germain_becker_ac-nantes_fr/ERepctxhM4BHt3L-dtIrJZoBPJrDt3L7-DP9iltqgCYbBw?e=31A7hd&amp;action=embedview&amp;wdAr=1.7777777777777777&amp;Embed=1" width="100%" height="640px" frameborder="0" allowfullscreen>This is an embedded <a target="_blank" href="https://office.com">Microsoft Office</a> presentation, powered by <a target="_blank" href="https://office.com/webapps">Office Online</a>.</iframe>   
</div>
<p class="impression">
    Source diaporama : <a href="https://dgxy.link/jouons_a_internet">https://dgxy.link/jouons_a_internet</a>
</p>

# Partie 3 : DNS

On a vu dans la partie précédente que pour qu'une machine A communique avec une machine B elle a besoin de connaître son adresse IP.

## Connaître sa propre adresse IP et celle de son DNS

<div class="a-faire titre" markdown="1">

### ✏️ À faire

Commencez par ouvrir l’invite de commandes de Windows. Pour cela :

1. Dans le menu Démarrer, tapez « cmd » ou « invite de commandes » sans appuyer sur la touche Entrée.
2. Puis cliquez sur l’icône Invite de commandes (ou cmd) qui ressemble à l'image ci-dessous : 

<img class="centre image-responsive" src="data/invite_de_commandes.png" alt="capture d'écran">

Répondez aux questions qui suivent.

</div>

💻✍️ **Question 1** : Tapez la commande `ipconfig` dans l’invite de commande puis validez en tapant Entrée (vous devriez obtenir un écran qui ressemble à celui ci-dessous). Trouvez dans les lignes qui s'affichent, l'adresse IP (version 4) de votre ordinateur.

<img class="centre image-responsive" src="data/ipconfig.png" alt="capture d'écran">

✍️ **Question 2** : Les ordinateurs d'un même réseau local ont des adresses IP qui commencent par les mêmes chiffres. Comparez votre adresse IP avec celles des ordinateurs de vos voisins. Que constatez-vous ?

💻✍️**Question 3** : Tapez maintenant la commande `ipconfig /all` dans l'invite de commande puis validez avec la touche Entrée. Cherchez dans les lignes qui apparaissent celle s'intitulant "Serveurs DNS" et notez l'adresse (ou les adresses) IP de votre (vos) serveurs DNS.

## Connaître l'adresse IP du destinataire

Comme il n'est pas simple pour un humain de retenir tout un tas d'adresses IP, on utilise plutôt des **adresses symboliques**. Ainsi, si on veut se connecter au site `www.lemonde.fr`, il suffit de taper `www.lemonde.fr` dans la barre d'URL du navigateur et on n'a pas besoin de connaître l'adresse IP du serveur qui héberge ce site. En effet, c'est le **serveur DNS** qui connaît les correspondances entre adresses IP et adresses symboliques et qui nous dira quelle est l'adresse IP du serveur hébergeant le site en question.

💻✍️ **Question 4** : Pour tester si une machine est joignable sur le réseau Internet, on peut utiliser la commande `ping`. Tapez la commande `ping www.lemonde.fr` dans l'invite de commandes et notez l'adresse IP de la machine `www.lemonde.fr` qui apparaît dans les informations affichées.

<blockquote class="information">
    <p>En théorie, on peut utiliser l'adresse IP dans la barre d'adresse du navigateur. Par exemple, si vous tapez <code>216.58.198.195</code> comme URL, vous vous retrouvez directement sur la page d'accueil du moteur de recherche Google. En pratique, cela ne marche pas avec tous les sites pour des raisons que nous n'évoquerons pas ici.</p>
</blockquote>

## Connaître le chemin parcouru dans le réseau

Pour connaître l'intinéraire suivi par les paquets entre l'expéditeur et le destinataire (les adresses IP et les noms des routeurs qui ont permis l’acheminement des paquets) on peut utiliser la commande `tracert`.

💻✍️**Question 5** : Tapez la commande `tracert www.lemonde.fr` (et validez avec Entrée). Vérifiez que la dernière ligne correspond bien à `www.lemonde.fr`. Par combien de routeurs les paquets sont-ils passés pour atteindre la machine `www.lemonde.fr` ?

## Bilan

💻✍️ **Question 6** : Compétez le schéma suivant en indiquant les adresses IP des ordinateurs, du serveur DNS et du serveur `www.lemonde.fr` (voir réponses précédentes). Annotez ensuite le schéma pour expliquer l'ordre des différentes communications entre les machines entre le moment où vous avez validé l'URL `www.lemonde.fr` dans le navigateur de votre ordinateur et le moment où la machine `www.lemonde.fr` a reçu votre demande.

<img class="centre image-responsive" src="data/serveur_DNS.png" alt="capture d'écran">

<blockquote class="question">
    <p>Selon vous, que renvoie la machine <code>www.lemonde.fr</code> et à qui ?</p>
</blockquote>

<div class="a-supprimer">

---
**Références** :

- Manuel de SNT, BORDAS, collection 3.0.

---
Les enseignants de SNT du lycée Emmanuel Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)

</div>