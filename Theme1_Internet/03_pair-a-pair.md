Les réseaux pair-à-pair
=======================

<blockquote class="information">
    <p><strong>Crédit</strong> : Cette séance est fortement inspirée de celle proposée par Louis Partenault sur les réseaux pair-à-pair : <a href="https://snt.ababsurdo.fr/internet/p2p/">https://snt.ababsurdo.fr/internet/p2p/</a>, publiée sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">CC BY-SA 4.0</a>.</p>
</blockquote>

La plupart des applications d'Internet reposent sur le **modèle client-serveur**. C'est le cas des serveurs Web ou encore des serveurs de messagerie électronique.
Dans ce modèle :
- le serveur est au centre et stocke toutes les informations
- tous les clients doivent se connecter au serveur pour lui demander un service (demander une page Web par exemple)

<img class="centre image-responsive" src="data/Server-based-network.svg" alt="capture d'écran" width="300">
<p class="legende">
    <strong>Un réseau de type client-serveur</strong>
    <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Server-based-network.svg">Mauro Bieg</a>, licence <a href="http://www.gnu.org/licenses/lgpl.html">LGPL</a>, via Wikimedia Commons.
</p>

On parle ainsi de modèle *centralisé*. Les clients peuvent néanmoins communiquer l'un avec l'autre mais toutes les informations passent par le serveur : par exemple, un client A publie une vidéo qui est stockée sur le serveur ; les autres clients peuvent accéder et lire cette vidéo à partir du serveur, le client B peut laisser un commentaire qui sera lui aussi stocké sur le serveur ; le client A et les autres clients peuvent voir les commentaires et y répondre, tout étant stocké sur le serveur, etc.

Il existe un autre modèle d'organisation, le **modèle pair-à-pair** (ou *peer-to-peer* en anglais, souvent abrégé *P2P*) dans lequel chaque ordinateur joue alternativement le rôle de client et de serveur.

<img class="centre image-responsive" src="data/P2P-network.svg" alt="capture d'écran" width="300">
<p class="legende">
    <strong>Un réseau pair-à-pair</strong>
    <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:P2P-network.svg">Mauro Bieg</a>, Domaine public, via Wikimedia Commons.
</p>

L'activité qui suit vous permet de comprendre le fonctionnement de ce modèle, les différences avec le modèle client-serveur, ainsi que les usages (licites et ilicites) que l'on peut en faire.

# Présentation et fonctionnement des réseaux pair-à-pair

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 1

Lisez <a href="https://fr.vikidia.org/wiki/Pair_%C3%A0_pair" target="_blank">cet article</a> (https://fr.vikidia.org/wiki/Pair_%C3%A0_pair) et répondez aux questions suivantes (elles suivent à peu près l'ordre de l'article donc n'hésitez pas à y répondre au fur et à mesure).

1. Pourquoi les réseaux pair-à-pair sont-ils considérés comme *décentralisés* ?
2. Pourquoi un réseau pair-à-pair est-il mieux protégé contre les pannes qu'un réseau de type client-serveur ?
3. Quelle est l'utilisation la plus répandue des réseaux pair-à-pair ? Pourquoi cela peut-il déboucher sur des conduites illégales ?
4. Quelle autre utilisation, souvent destinée à la recherche scientifique, peut-on faire des réseaux pair-à-pair ?

</div>

<div class="a-faire titre" markdown="1">

### ✏️ Exercice 2

Regardez la vidéo ci-dessous puis répondez aux questions suivantes.

<div class="video-responsive">
    <div class="youtube_player centre" videoID="6ZazbI8faqw" width="560" height="315" theme="light" rel="1" controls="1" showinfo="1" autoplay="0"></div>
</div>
<p class="impression">
    Source : ▶️ <a href="https://youtu.be/6ZazbI8faqw">https://youtu.be/6ZazbI8faqw</a>
</p>

1. Dans le cas d'un réseau pair-à-pair de transfert de fichiers, dans quel cas un ordinateur joue-t-il le rôle de client et dans quel cas joue-t-il le rôle de serveur ? 
2. Pourquoi le téléchargement de fichiers via un réseau pair-à-pair est-il plus rapide (qu'un téléchargement avec une application client-serveur) ?

</div>

# Une mauvaise image mais un regain d'usage

Le mode pair-à-pair a beaucoup été utilisé dans les années 2000 pour des partages de fichiers (notamment de la musique, des films, des jeux-vidéos) car les débits des connexions Internet étaient bien plus faibles qu'aujourd'hui. Comme les fichiers populaires étaient disponibles sur les ordinateurs de beaucoup d'internautes qui agissent comme autant de serveurs, la vitesse de téléchargement était plus élevée.

Les réseaux pair-à-pair sont tout à fait légaux, mais ils ont contribué à amplifier les téléchargements illégaux car la plupart des fichiers partagés sur ces réseaux pair-à-pair étaient protégés par le droit d'auteur. C'est pour cette raison qu'à été créé l'organisme <a href="https://fr.wikipedia.org/wiki/Haute_Autorit%C3%A9_pour_la_diffusion_des_%C5%93uvres_et_la_protection_des_droits_sur_internet" target="_blank">HADOPI</a> en 2009 dont le but principal est de lutter contre le "piratage", notamment par la surveillance des réseaux pair-à-pair. 

Les réseaux pair-à-pair ont donc longtemps été associés au partage illégal, mais les choses sont en train de changer. Par exemple, Microsoft utilise le mode pair-à-pair pour accélérer la distribution des mises à jour de Windows 10. Mais c'est surtout la technologie **blockchain** qui a remis les réseaux pair-à-pair sur le devant de la scène. En effet, blockchain utilise le mode pair-à-pair, et c'est avec cette technologie que sont sécurisées les monnaies virtuelles comme le *Bitcoin*, l'*Ethereum*, etc., et que l'on envisage de décentraliser l'infrastructure du <a href="https://fr.wikipedia.org/wiki/Web3" target="_blank">Web3</a> (voir aussi <a href="https://fr.wikipedia.org/wiki/Red%C3%A9centralisation_d%27Internet" target="_blank">Redécentralisation d'Internet</a>).

<img class="centre image-responsive" src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c5/Bitcoin_logo.svg/langfr-1920px-Bitcoin_logo.svg.png" alt="logo bitcoin" width="400">
<p class="legende">
    <strong>Logo du BitCoin</strong>
    <br>Crédit : <a href="https://commons.wikimedia.org/wiki/File:Bitcoin_logo.svg">Bitboy</a>, Domaine public, via Wikimedia Commons.
</p>

<div class="a-supprimer">

---
**Références** :

- Manuel de SNT, BORDAS, collection 3.0.
- Séance de Louis Partenault sur les réseaux pair-à-pair : <a href="https://snt.ababsurdo.fr/internet/p2p/" target="_blank">https://snt.ababsurdo.fr/internet/p2p/</a>, diffusé sous licence <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr" target="_blank">CC BY-SA 4.0</a>

---
Les enseignants de SNT du lycée Emmanuel Mounier, ANGERS ![Licence Creative Commons](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)

</div>