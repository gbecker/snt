# SNT

Dépôt pour les cours et activités de SNT au lycée Emmanuel Mounier à Angers.

Ce dépôt sera alimenté au fur et à mesure.

Le site utilisé par les élèves est : https://info-mounier.fr/snt/

Sauf mention contraire, tous les documents présents sur ce dépôt sont sous licence CC-BY-NC-SA

![Licence Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)